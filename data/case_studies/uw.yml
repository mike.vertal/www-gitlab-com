title: University of Washington
cover_image: '/images/blogimages/uw-case-study-image.png'
cover_title: |
  The Paul G. Allen Center for Computer Science & Engineering gains control and flexibility to easily manage 10,000+ projects.

customer_logo: 'images/uw-logo.svg'
customer_logo_css_class: brand-logo-wide
customer_industry: Public sector - University
customer_location: Seattle, Washington, USA
customer_employees: 1,500 users

key_benefits:
  - |
    GitLab has scaled to effectively host over 10,000 projects, representing roughly
    400 GB of data, on one instance with the same speed and quality of the first project
  - |
    1 software engineer on the IT team supports approximately 2,000 users
  - |
    Faculty values it as a contemporary platform with which to
    teach courses and introduce current software development concepts to students
  - |
    For the local IT team, it’s a place to collect and organize source code for all custom tools developed by the team

customer_quote:
  quote_position: top
  quote_text: |
    Over the past two years, GitLab has been transformational for our organization here at UW.  Your platform is fantastic!
  customer_name: Aaron Timss
  customer_title: Director of Information Technology, CSE

customer_study_content:
  - title: THE CUSTOMER
    content:
        - |
          The Paul G. Allen Center for Computer Science &
          Engineering (the Allen School) at the University of Washington (UW) is consistently ranked as one of the top computer
          science and computer engineering programs in the US. The school is in the midst
          of a dramatic expansion as the demand for computer science degrees has never
          been higher. The Allen School currently consists of nearly 1,000
          undergraduates, 250 PhD students, and 70 faculty members who leverage version
          control and continuous integration tools to complete everything from class
          assignments to open source research and educational outreach projects.
          Aaron Timss, Director of Information Technology at the Allen School, leads a
          team of 20 software engineers and technical staff who are responsible for
          selecting and optimizing the tech stack that supports the school's research,
          instructional, and development needs.

  - title: THE CHALLENGE
    content:
      - |
        For years, the Allen School had been using Subversion (SVN) in conjunction
        with some homegrown scripts to support a flat, ad hoc version control system.
        However, faculty and students were getting frustrated with the slowness and
        workflow limitations of SVN, and having to rely on arcane Linux scripts to
        manage permissions on group directories and repositories. Fed up with SVN, many
        students and faculty quietly turned to online Git repository managers, such as
        GitHub, to host their course assignments and collaborate on projects. But in
        some cases, students were inadvertently leaving their class assignments ‘public,’
        violating university policy. The Allen School had world-facing open source
        projects. The critical requirement was to find a solution which provided
        federated login for external collaboration, while ensuring protections on
        sensitive projects such as student coursework and unpublished research.

      - |
        Faced with the challenge above and a rapidly growing user base, it was clear that
        the Allen School IT team needed to find a solution that met both the collaboration
        and security requirements of their students and faculty. The team initially
        considered a number of self-hosted options, including GitHub Enterprise; however,
        this platform didn't provide an easy way for research teams to share and collaborate
        on their open source projects externally with other investigators or institutions.
        After some additional research, in the fall of 2014, the team decided to move
        forward with GitLab.

  - title: THE RESULTS
    content:
      - |
        The Allen School has now been using GitLab for over two years and they recently
        surpassed the milestone of their 10,000th project. Jason Howe, a Software
        Engineer on the school's IT team, led the process of making GitLab available
        to the school’s students and faculty. Six months after rolling GitLab out,
        Jason authored provisioning tools on top of GitLab to enable faculty members
        to easily add students to course-specific projects. As more students started
        to use it as part of their course work, adoption climbed, and the number
        of projects on GitLab skyrocketed.

      - |
        IT staff noticed that students were now choosing to host more of their personal
        development projects on the service, and adoption by Allen School instructors
        jumped from the initial handful of early adopters to a few dozen. They also
        started to see interest in the platform from neighboring units within the
        university. The IT team is extremely pleased with GitLab’s ability to easily
        scale with the demands of their growing organization, and they appreciate
        GitLab’s fervor for further developing the platform and staying on top of
        security issues. Even as usage of GitLab continues to grow, Jason and the
        team still find the product easy to maintain. They spend just 1-2 days per
        quarter updating and maintaining GitLab.

      - |
        The Allen School’s students, faculty, and IT team are happy with their decision
        to choose GitLab. Jason sums up GitLab’s benefits in two words: control and
        flexibility. From an admin or systems perspective, GitLab gives the IT team
        the necessary controls to ensure that sensitive university research and
        students’ coursework are all easily manageable and kept safe. And in terms of
        flexibility, GitLab is open source and that makes it possible for the IT team
        to build unique SSO and provisioning tools against GitLab’s API.
