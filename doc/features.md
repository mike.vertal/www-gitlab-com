# Update the features

All features are listed in a single yaml file
([`/data/features.yml`](/data/features.yml)) under the `features` section.
It is the single source of truth for the following pages:

- <https://about.gitlab.com/features/>
- <https://about.gitlab.com/products/>
- <https://about.gitlab.com/gitlab-com/>
- <https://about.gitlab.com/comparison/>
- <https://about.gitlab.com/roi/>

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Features attributes](#features-attributes)
- [Update the features page (under `/features`)](#update-the-features-page-under-features)
- [Update the products page (under `/products`)](#update-the-products-page-under-products)
- [Update the gitlab-com page (under `/gitlab-com`)](#update-the-gitlab-com-page-under-gitlab-com)
- [Create or update the comparison pages (under `/comparison`)](#create-or-update-the-comparison-pages-under-comparison)
- [Create or update the solutions pages (under `/solutions`)](#create-or-update-the-solutions-pages-under-solutions)
- [Update the Return on Investment calculator page (under `/roi`)](#update-the-return-on-investment-calculator-page-under-roi)
- [Why we use YAML for the solutions, features, and pricing pages](#why-we-use-yaml-for-the-solutions-features-and-pricing-pages)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

---

## Features attributes

The following table depicts all possible attributes a feature can have and the
page they're used on. These are the attributes under the `features:` hash in
[`data/features.yml`](/data/features.yml).

**Note:** You don't have to include all of them when you add a new feature or
edit an existing one. Just pick those values according to the "Used on page(s)"
column.

| Feature attribute  | Used on page(s) | Description |
| -------------------| --------------- | ----------- |
| `title`            | `/features`, `/products`, `/gitlab-com`, `/comparison`, `/roi` | The distinct title of a feature.  |
| `description`      | `/features`, `/products`, `/gitlab-com`, `/comparison` | The description of the feature. Can include Markdown. |
| `screenshot_url`   | `/features` | Optional. Relative URL of the feature screenshot if relevant. |
| `link_description` | `/features`, `/comparison` |  Below every feature usually a link appears pointing to the feature page or the documentation. This value is its description, e.g., `"Learn more about CI/CD"`. In pages where it's not used, a generic description may appear. |
| `link`             | `/features`, `/products`, `/gitlab-com`, `/comparison` | The link pointing either to the feature page (relative link, e.g., `/features/pages`), the docs or an issue on the issue tracker the feature is being worked on. |
| `feature_page`     | `/features`, `/products` | Optional. Set to true for pages that have their own dedicated page under `/features`. If set to true, the `link` should point to the relevant feature page. All features that have this and `gitlab_libre` set to true, are shown on `/products` under CE (because CE has about as many features in the `features.yml` as EES, it makes EES look less enticing, so we currently only show features that have a feature page). |
| `solution`         | `/features` | Every feature must have a solution (category) otherwise CI will fail. Check the [`data/solutions.yml`](../data/solutions.yml) file for a list of existing ones. When in doubt, ask in the `product` chat channel. |
| `gitlab_libre`     | `/features`, `/products`, `/gitlab-com` | `true` if the feature is in CE, `false` if not, `'partially'` if partially supported (generally avoid using 'partially'). |
| `gitlab_starter`   | `/features`, `/products`, `/gitlab-com`, `/comparison`  | `true` if the feature is in EES/GitLab.com Bronze, `false` if not, `'partially'` if partially supported (generally avoid using 'partially'). |
| `gitlab_premium`   | `/features`, `/products`, `/gitlab-com`, `/comparison` | `true` if the feature is in EEP/GitLab.com Silver, `false` if not, `'partially'` if partially supported (generally avoid using 'partially'). |
| `gitlab_ultimate`  | `/features`, `/products`, `/gitlab-com`, `/comparison` | `true` if the feature is in GitLab Ultimate/Gold, `false` if not, `'partially'` if partially supported (generally avoid using 'partially'). |
| `gitlab_com`       | `/features`, `/products`, `/gitlab-com`, `/comparison` | `true` if the feature is available on GitLab.com, `false` if not. Used for features which users don't have access to on GitLab.com, for example LDAP authentication or admin settings. If not set, it's `true` by default. |
| `github_com`       | `/comparison` | `true` if the feature is in GitHub.com, `false` if not. To be used with `gitlab_premium` and `gitlab_com`. |
| `github_enterprise` | `/comparison` | `true` if the feature is in GitHub Enterprise, `false` if not. To be used with `gitlab_premium`. |
| `bitbucket_org`    | `/comparison` | `true` if the feature is in Bitbucket.org, `false` if not. To be used with `gitlab_com`. |
| `bitbucket_server` | `/comparison` | `true` if the feature is in Bitbucket Server, `false` if not. To be used with `gitlab_premium`. |
| `gitlab_ci`        | `/comparison` | `true` if the feature is in GitLab CI/CD, `false` if not. To be used with `travis_ci`, `circle_ci` and `jenkins`. |
| `travis_ci`        | `/comparison` | `true` if the feature is in Travis CI, `false` if not. To be used with `gitlab_ci`. |
| `jenkins`          | `/comparison` | `true` if the feature is in Jenkins, `false` if not. To be used with `gitlab_ci`. |
| `circle_ci`        | `/comparison` | `true` if the feature is in Circle CI, `false` if not. To be used with `gitlab_ci`. |
| `svn`              | `/comparison` | `true` if the feature is in SVN, `false` if not. To be used with `gitlab_premium`. |
| `saas`             | `/comparison` | `true` if the feature is in SaaS, `false` if not. To be used with `gitlab_premium`. |
| `gitlab_issue_boards` | `/comparison` | `true` if the feature is in GitLab Issue Boards, `false` if not. To be used with `trello` and `asana`. |
| `trello`           | `/comparison` | `true` if the feature is in Trello, `false` if not. To be used with `gitlab_issue_boards`. |
| `asana`            | `/comparison` | `true` if the feature is in Asana, `false` if not. To be used with `gitlab_issue_boards`. |
| `gitlab_pages`     | `/comparison` | `true` if the feature is in GitLab Pages, `false` if not. To be used with `github_pages`. |
| `github_pages`     | `/comparison` | `true` if the feature is in GitHub Pages, `false` if not. To be used with `gitlab_pages`. |
| `github_pull_requests`  | `/comparison` | `true` if the feature is in GitHub Pull Requests, `false` if not. To be used with `gitlab_merge_requests`. |
| `gitlab_merge_requests` | `/comparison` | `true` if the feature is in GitLab Merge Requests, `false` if not. To be used with `github_pull_requests`. |
| `shorthand`         | `/roi` | This is the id that it's used for the url parameters so users can share their ROI calculations. It needs to have underscores (`_`) instead of spaces in order for it to work.  |
| `hours_per_incident`| `/roi` | The number of hours that an incident will take before it's solved. It can be skipped but it will make the calculator return 0 for that feature. |
| `incidents_per_year`| `/roi` | The number of times that an incident will happen in a year. It can be skipped but it will make the calculator return 0 for that feature. |
| `roi_case`          | `/roi` |The description of the case that makes use of the feature that's described on the title, if this is skipped the feature will not be considered for the roi features table. |

### Badges

The badges in `features/`, `comparison/`, and `release-posts` display the
feature availability in [GitLab Tiers](https://about.gitlab.com/handbook/marketing/product-marketing/#tiers).

Use the following pattern to apply the correct badge to the feature (Libre, Starter, Premium, Ultimate):

- `features.yml`:
  - For GitLab Libre, use `gitlab_libre: true`, `gitlab_starter: true`, `gitlab_premium: true`, `gitlab_ultimate: true`
  - For GitLab Starter, use `gitlab_starter: true`, `gitlab_premium: true`, `gitlab_ultimate: true`
  - For GitLab Premium, use `gitlab_premium: true`, `gitlab_ultimate: true`
  - For GitLab Ultimate, use `gitlab_ultimate: true`
- Release posts: see the [Release Posts handbook](https://about.gitlab.com/handbook/marketing/blog/release-posts/#feature-availability).

If the feature is available in GitLab.com, the badges for GitLab.com will be
applied automatically according to the self-hosted availability. For example,
if `gitlab_premium: true`, `gitlab_ultimate: true`, it will "turn on" the
badges Premium, Ultimate, Silver, and Gold.

If the feature is not available in GitLab.com, e.g., LDAP and admin settings,
use the tag `gitlab_com: false` to turn off the entire GitLab.com badges' row.
For example,
if `gitlab_premium: true`, `gitlab_ultimate: true`, and `gitlab_com: false`,
it will "turn on" the badges Premium and Ultimate, and turn off the entire
GitLab.com badges row.

## Update the features page (under `/features`)

The [`/features`](https://about.gitlab.com/features/) page grabs its content
automatically from [`/data/features.yml`](/data/features.yml).

Consult the [features attributes table](#features-attributes) for a complete
list of possible attributes.

Example:

```yaml
  - title: "Distinct feature name"
    description: "Feature description"
    screenshot_url: "images/feature_page/screenshots/09-gitlab-ci.png"
    link_description: "Learn more about feature name"
    link: link to docs or feature page
    solution: efficiency
    gitlab_libre: true
    gitlab_starter: true
    gitlab_premium: true
    gitlab_ultimate: true
    gitlab_com: false
```

## Update the products page (under `/products`)

The [`/products`](https://about.gitlab.com/products/) page grabs its content
automatically from [`/data/features.yml`](/data/features.yml).

Consult the [features attributes table](#features-attributes) for a complete
list of possible attributes.

## Update the gitlab-com page (under `/gitlab-com`)

The [`/gitlab-com`](https://about.gitlab.com/gitlab-com/) page grabs its content
automatically from [`/data/features.yml`](/data/features.yml).

Consult the [features attributes table](#features-attributes) for a complete
list of possible attributes.

## Create or update the comparison pages (under `/comparison`)

The [comparison page](https://about.gitlab.com/comparison/) grabs its content
automatically from [`/data/features.yml`](/data/features.yml).

There are 2 files in total which you need to create or update:

- `data/features.yml`: Update for new comparisons. Comparisons are automatically
  generated from the contents of this file. Consult the
  [features attributes table](#features-attributes) for a complete list of
  possible attributes.
- `source/comparison/gitlab-vs-competitor.html.haml`: Create for new comparisons.
  Every comparison page has its own HTML file (**use dashes**).

1. Edit `data/features.yml` (`competitors`, `comparisons` and `features`
   sections). For example:

    ```yaml
    competitors:
      gitlab_com:
        name: 'GitLab.com'
        logo: '/images/comparison/gitlab-logo.svg'
      github_com:
        name: 'GitHub.com'
        logo: '/images/comparison/github-logo.svg'

    ...

    comparisons:
      gitlab_com_vs_github_com:
        title: 'GitLab.com Silver vs. GitΗub.com'
        link: '/comparison/gitlab-com-vs-github-com.html'
        product_one: 'gitlab_com'
        product_two: 'github_com'
        pdf: true

    ...

    features:
      - title: "Briefly explain the feature"
        description: |
          Describe the differences in detail. This text can span in multiple
          lines without interfering with its structure. It will always appear
          as one paragraph.
        link_description: "Learn more about Feature Name."
        link: "link to GitLab's feature page documentation or blog post"
        gitlab_com: true
        github_com: true
    ```

      **Notes:**
      - The `link` attribute is the relative link to the comparison page. It should
        have the same name as the one we create in the next step.
      - If the `pdf` is set to true, it will add a link to the PDF at the bottom
        of the comparison page. The PDF name will be the same as the comparison
        HTML file. Note that the PDF is **not** generated automatically, you
        will have [to create it](../pdf.md#comparison-pdfs). Remove this entry
        altogether to omit the PDF link creation.
      - The competitor's logo (`product_two`) can be `svg` or `png`. Save it in
        `source/images/comparison/competitor-logo.svg`.
      - In the features area, `product_one` is always GitLab, and `product_two`
        is the competitor we are comparing against. Values for these two fields are
        `true|false|partially`.

2. Add a new haml file named after the comparison using dashes:
   `source/comparison/gitlab-vs-competitor.html.haml`. You can start by copying
   an existing one and then editing it. The only change you need to make is the
   names of competitors:

    ```yaml
    ---
    layout: comparison_page
    trial_bar: true
    suppress_header: true
    title: "GitHub.com vs GitLab.com | GitLab compared to other tools"
    image_title: '/images/comparison/title_image.png'
    extra_css:
      - compared.css
    extra_js:
      - comparison.js
    ---

    - competitors = data.features.competitors
    = partial "includes/comparison_table", locals: { comparison_block: data.features.comparisons.gitlab_com_vs_github_com, product_one: competitors.gitlab_com, product_two: competitors.github_com, key_one: :gitlab_com, key_two: :github_com }
    ```

1. If you followed the above 2 steps, the new comparison page should be reachable
   under `/comparison/gitlab-vs-competitor.html` and you should see it in the
   dropdown menu. The last thing you need to do is create the PDF. Follow the
   info in [creating comparison PDFs](pdf.md#comparison-pdfs).

## Create or update the solutions pages (under `/solutions`)

Every feature should have a solution. The list of the existing solutions can be
found at [`data/solutions.yml`](/data/solutions.yml).

To create a new one solution:

1. Edit [`data/solutions.yml`](/data/solutions.yml) and add the new solution.
   You can copy the format of an existing one.

    >**Note:**
    There are two categories of solutions: 1) "Phases of the software development
    lifecycle" and 2) "Quality attributes of GitLab". The features index page
    lists them both. The first ones are taken from [`data/solutions.yml`](/data/solutions.yml),
    so when you add a new solution that belongs to the "Phases of the software
    development lifecycle", make sure to also update the value in
    `source/features/index.html.haml` (`data.solutions.solutions.take(7).each` and
    `data.solutions.solutions.drop(7).each`).

1. Create `source/solutions/<solution-name>/index.html.haml`. You can copy the
   format of an existing solution to get started.

To update an existing solution, just edit [`data/solutions.yml`](/data/solutions.yml).

## Update the Return on Investment calculator page (under `/roi`)

The [`/roi`](https://about.gitlab.com/roi/) page grabs its content
automatically from [`/data/features.yml`](/data/features.yml).

Consult the [features attributes table](#features-attributes) for a complete
list of possible attributes.

## Not yet implemented features

Features that are not yet implemented and not yet scheduled,
but are interesting to GitLab, should be documented in `features.yml`. 
Include at least the following attributes: 

```
- title
- description
- link_description
- link (Link to the issue)
- solution: missing
```

The `solution: missing` attribute and value will display the feature in the 
Missing section of <https://about.gitlab.com/features/>.

## Why we use YAML for the solutions, features, and pricing pages

We decided to use YAML after attempting to maintain the features, pricing, and
solutions pages independently of one another. This didn't work well because the
pages often had overlapping content, and the site wasn't following
[DRY principles](https://en.wikipedia.org/wiki/Don%27t_repeat_yourself). Without
being DRY, we frequently repeated work unnecessarily and the pages became increasingly
difficult to maintain, especially with our rapid release schedule.

To prevent this from happening again, we want to emphasize these problems
and have people update the YAML files instead of the respective Haml files.

For more information, see issues [#1334](https://gitlab.com/gitlab-com/www-gitlab-com/issues/1334)
and [#1478](https://gitlab.com/gitlab-com/www-gitlab-com/issues/1478).
