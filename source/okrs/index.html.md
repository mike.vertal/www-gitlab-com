---
layout: markdown_page
title: "Objectives and Key Results (OKRs)"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## What are OKRs?

OKRs are our quarterly goals to execute our [strategy](https://about.gitlab.com/strategy/). To make sure our goals are clearly defined and aligned throughout the organization. For more information see [Wikipedia](https://en.wikipedia.org/wiki/OKR) and [Google Drive](https://docs.google.com/presentation/d/1ZrI2bP-XKEWDWsT-FLq5piuIwl84w9cYH1tE3X5oSUY/edit) (GitLab internal). The OKRs are our quarterly goals.

## Format

Before the quarter:

`Owner: Objective as a sentence. Key result, key result, key result.`

During and after the quarter:

`Owner: Key Result as a sentence. Key result, key result, key result. => Outcome, outcome, outcome.`

- Each objective has between 1 and 3 key results.
- Each key result has an outcome.
- Owner is the title of role that will own the result.
- We use four spaces to indent instead of tabs.
- The key result can link to an issue.
- The outcome can link to real time data about the current state.
- The three CEO objectives are level 3 headers to provide visual separation.

## Levels

We only list objectives prefaced with your role title.
We do OKRs up to the team or director level, we don't do [individual OKRs](https://hrblog.spotify.com/2016/08/15/our-beliefs/).
Part of the individual performance review is the answer to: how much did this person contribute to the team objectives?
We have no more than [five layers in our team structure](https://about.gitlab.com/team/structure/).
Because we go no further than the manager level we end up with a maximum 4 layers of indentation on this page.
The match of one "nested" key result with the "parent" key result doesn't have to be perfect.
Every owner should have at most 3 objectives. To make counting easier always mention the owner with a trailing colon, like `Owner:`.
The advantage of this format is that the OKRs of the whole company will fit on three pages, making it much easier to have an overview.

## Updating

The key results are updated continually throughout the quarter when needed.
Everyone is welcome to a suggestion to improve them.
To update: make a merge request and assign it to the CEO.
If you're a [team member](https://about.gitlab.com/team/) or in the [core team](https://about.gitlab.com/core-team/) please post a link to the MR in the #okrs channel and at-mention the CEO.

At the top of the OKRs is a link to the state of the OKRs at the start of the quarter so people can see a diff.

Timeline of how we draft the OKRs:

1. CEO pushes top goals to this page: 5 weeks before the start of the quarter
1. Executive team pushes updates to this page: 4 weeks before the start of the quarter
1. Executive team 90 minute planning meeting: 3 weeks before the start of the quarter
1. Discuss with the board and the teams: 2 weeks before the start of the quarter
1. Executive team 'how to achieve' presentations: 1 week before the start of the quarter
1. Add Key Results to top of 1:1 agenda's: before the start of the quarter
1. Present OKRs at a functional group update: first week of the quarter
1. Present 'how to achieve' at a functional group update: during first three weeks of the quarter
1. Review previous quarter and next during board meeting: after the start of the quarter

## Scoring

It's important to score OKRs after the quarter ends to make sure we celebrate what went well, and learn from what didn't in order to set more effective goals and/or execute better next quarter.

1. Move the current OKRs on this page to an archive page _e.g._ [2017 Q3 OKRs](/okrs/2017-q3/)
1. Add in-line comments for each key result briefly summarizing how much was achieved _e.g._
  * "=> Done"
  * "=> 30% complete"
1. Add a section to the archived page entitled "Retrospective"
1. OKR owners should add a subsection for their role outlining...
  * GOOD
  * BAD
  * TRY
1. Promote the draft OKRs on this page to be the current OKRs

## Critical acclaim

Spontaneous chat messages from team members after introducing this format:

> As the worlds biggest OKR critic, This is such a step in the right direction :heart: 10 million thumbs up

> I like it too, especially the fact that it is in one page, and that it stops at the team level.

> I like: stopping at the team level, clear reporting structure that isn't weekly, limiting KRs to 9 per team vs 3 per team and 3 per each IC.

> I've been working on a satirical blog post called called "HOT NEW MANAGEMENT TREND ALERT: RJGs: Really Just Goals" and this is basically that. :wink: Most of these are currently just KPIs but I won't say that too loudly :wink: It also embodies my point from that OKR hit piece: "As team lead, it’s your job to know your team, to keep them accountable to you, and themselves, and to be accountable for your department to the greater company. Other departments shouldn’t care about how you measure internal success or work as a team, as long as the larger agreed upon KPIs are aligned and being met."

> I always felt like OKRs really force every person to limit freedom to prioritize and limit flexibility. These ones fix that!

## Hiring as an objective

The complete hiring plan is kept in the Hiring Forecast doc. Hiring is not an objective in-and-of-itself. However hiring critical members for a team can be considered a key result. This is because recruiting top technical talent in a competitive startup environment can consume a large proportion of management's time and those hires are modeled into our product development goals. Keep all hiring-related KR's in the 'Team' objective.

## 2018-Q1

### Objective 1: Grow Incremental ACV according to plan

* CEO: IACV doubles year over year
  * VP Product:
  * CRO
    * Customer Success: Identify success factors
    * Customer Success: Do quarterly business reviews for all eligible customers
    * Sales: Add growth pipeline of 1.5x annual growth plan
    * Sales: Add 30 Fortune 500 companies
* CEO: Be at a sales efficiency of 1.0 or higher
  * CMO
    * Marketing: know cost per SQO and customer for each of our campaigns
* CEO: Make sure that 70% of salespeople are at 70% of quota
  *CMO
    * Marketing: Make sure each SAL has 10 SAO's per month
  * CRO
    * Sales: Increase IACV by 15% for Strategic / Large / Mid Market
    * Sales: 1 month boot-camp for sales people with rigorous testing
    * Sales: Professional Services in 50% of Strategic / Large deals
  * VPE
    * Support: 100% Premium and Ultimate SLA achievement
  * CFO
    * Legal: Implement improved contract flow process for sales assisted opportunities
    * Controller: Billing support added for EMEA region.
    * Legal: GDPR policy fully implemented.
  * CMO: Establish credibility and thought leadership with Enterprise Buyers delivering on pipeline generation plan through the development and activation of integrated marketing and sales development campaigns:
    * MSD: Scale sales development organization hiring to plan, accelerating onboarding and getting reps productive to deliver on SCLAU growth plans.
    * MSD: achieve volume target in inbound SCLAU generation.
    * MSD: achieve volume target in outbound SCLAU generation.
    * MSD: develop and execute Automate to accelerate CI; Kubernetes and Concurrent DevOps campaigns.
    * PMM: Activate category strategy, positioning and messaging with sales enablement and certification program and website content.
    * PMM: Develop and roll out updated pitch and analyst decks
    * PMM: CE to EE Pitch Deck and SVN to EE pitch Deck
  * CMO: Website redesign iteration, including information architecture update, to support our awareness and lead generation objectives, accounting for distinct audiences.
  * CMO: Further develop thought leadership platforms for GitLab around topics including forecasting the future of development, redefining cultural excellence, and helping to make security an actionable priority for developers.

### Objective 2: Popular next generation product

* CEO: GitLab.com ready for mission critical workloads
  * VPE: Move GitLab.com to GKE
    * Geo: Make Geo performant to work at GitLab.com scale
    * Distribution: TBD?
    * Gitaly: TBD?
    * CI/CD: TBD?
  * VPE: GitLab.com available 99.95% and monthly disaster recovery exercises
  * VPE: GitLab.com speed index < 1.5s for all tested pages
  * Product: Ship group-level authentication
* CEO: On track to deliver all features of [complete DevOps](https://about.gitlab.com/2017/10/11/from-dev-to-devops/)
  * VPE: Ship faster than before
  * Product: Plan all features to be done by August 22
  * VPE: [One codebase with /ee subdirectory](https://gitlab.com/gitlab-org/gitlab-ee/issues/2952)
* CEO: Make it popular
  * Marketing: Get unique contributors per release to 100
  * Marketing: Increase total users by 5% per month
  * Marketing: Facilitate 100 ambassador events (meetups, presentations)
  * Marketing: Be a leader in all relevant analyst reports
  * Product: Grow usage of security features to over 1000 projects
  * Product: Grow usage of portfolio management features to over 1000 projects
  * VPE: Use all of GitLab ourselves (monitoring, release management)
  * Director of Backend
    * Ensure SP1/SP2 issues for top tier customer get fixed
  * UX
    * UX: [Reduce the installation time of devops for Kubernetes by 50%](https://gitlab.com/groups/gitlab-org/-/epics/33)
    * UX: [Establish Operation Engineers as a first class citizen. Create a roadmap for Operations to use gitlab as part of their core stack on a day to day basis.](https://gitlab.com/groups/gitlab-org/-/epics/47)
    * UX: [Complete design pattern library, setting usability standards and solutions for design, development, and product management to implement and follow.](https://gitlab.com/groups/gitlab-org/-/epics/29)
  * Geo
    * Geo: Make [Geo Disaster Recovery](https://gitlab.com/gitlab-org/gitlab-ee/issues/846) Generally Available
      * 100% (Done in 10.5)
    * Geo: Squash 15+ bugs/month
       * [27 open bugs](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Geo&label_name[]=bug)
       * 10.4: [8 closed bugs](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=closed&label_name[]=Geo&label_name[]=bug&milestone_title=10.4)
       * 10.5: [13 closed bugs](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=closed&label_name[]=Geo&label_name[]=bug&milestone_title=10.5)
       * 10.6: [12 closed bugs](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=Geo&milestone_title=10.6&page=2&scope=all&sort=created_date&state=closed)
    * Geo: Bring `ee/app/workers` and `ee/db/migrate` directories up to 95% coverage
       * Geo-related files [show 95% coverage](http://gitlab-org.gitlab.io/gitlab-ee/coverage-ruby/#_AllFiles)
    * Geo: Deliver 100% of feature commits in 10.5, 10.6
       * 10.5: 100% (delivered DR for 1-primary 1-secondary configuration, demoed 1-primary N-secondary work)
       * 10.6: 75% (first iteration of repo verification, planned failover documentation, missed cleanup for files moved to object storage)
  * Distribution
    * Distribution: Upgrade omnibus and internal omnibus-gitlab Chef
      * 100% ([Epic](https://gitlab.com/groups/gitlab-org/-/epics/35))
    * Distribution: Measure upgrade/installation time between two GitLab versions.
      * 100% ([Epic](https://gitlab.com/groups/gitlab-org/-/epics/36))
    * Distribution: Establish a roadmap for automated vulnerability reporting of shipped libraries
    * Distribution: Cloud Native Helm charts in Alpha
    * Distribution: Support for generating LE certificates from the omnibus-gitlab package
      * 100% ([Issue](https://gitlab.com/gitlab-org/omnibus-gitlab/issues/2620))
    * Distribution: Ship 100% of committed deliverables issues each release
      * 10.5: 87.5% (One undelivered issue was
      caused by [the change in direction](https://gitlab.com/gitlab-org/omnibus-gitlab/issues/3002#note_57574482))
      * 10.6: 70% (Did not ship LE on by default, did not make significant progress on the backup/restore task fix)
  * Platform
    * Platform: Ship 100% of committed [deliverable issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Platform&label_name[]=backend&label_name[]=Deliverable) each release
        * 10.5: [24/32 (75%)](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/9935)
    * Platform: Resolve all [Security SL1](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Platform&label_name[]=backend&label_name[]=SL1), [Support SP1](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Platform&label_name[]=backend&label_name[]=SP1), and [Availability AP1](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Platform&label_name[]=backend&label_name[]=AP1) issues
    * Platform: Close 45 [Platform backend bug](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=Platform&label_name%5B%5D=backend&label_name%5B%5D=bug&scope=all&sort=updated_desc&state=opened) issues. Afterwards, we should verify that the backlog went _down_ from the 400 we started with, because otherwise bugs are getting reported faster than we can fix them, and we are not making a dent.
    * Platform: Add [backup/restore integration tests](https://gitlab.com/gitlab-org/gitlab-qa/issues/22) to GitLab QA
    * Platform: Make sure all [Platform backend community contributions](https://gitlab.com/groups/gitlab-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Community%20Contribution&label_name[]=Platform) are merged, closed, labeled “awaiting feedback”, or taken over by us and in active development
  * Discussion
    * Discussion: Ship 100% of committed
      [deliverable issues][discussion-deliverables] each release.
      * [10.5: 70% (7/10)][discussion-10-5-update].
      * [10.6: 77% (7/9)][discussion-10-6-update].
      * Try: break large performance Deliverable into smaller issues.
    * Discussion: Make it possible to run GitLab as a Rails 5 app from the
      master branch.
    * Discussion: Resolve all [Security SL1][discussion-sl1],
      [Support SP1][discussion-sp1], and [Availability AP1][discussion-ap1]
      issues.
    * Discussion: Close 36 [Discussion backend bug][discussion-backend-bug]
      issues. Afterwards, we should verify that the backlog went _down_ from the
      280 we started with (and compare to the total of 385), because otherwise
      bugs are getting reported faster than we can fix them, and we are not
      making a dent.
      * 10.5: [18 bugs closed](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=closed&label_name[]=Discussion&label_name[]=backend&label_name[]=bug&milestone_title=10.5).
      * 10.6: [15 bugs closed](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=closed&label_name[]=Discussion&label_name[]=backend&label_name[]=bug&milestone_title=10.6).
    * Discussion: Make sure all of the 17
      [Discussion backend community contributions][discussion-backend-community-contributions]
      that were created before 1 April 2017 are merged, closed, labeled
      "awaiting feedback", or taken over by us and in active development.
  * CI/CD
    * CI/CD: Ship 100% of committed deliverables issues each release:
      * 10.5: [(25 / 40): 63%](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=CI%2FCD&label_name[]=Deliverable&milestone_title=10.5)
      * 10.6: [(21 / 33): 63%](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=CI%2FCD&label_name[]=Deliverable&milestone_title=10.6)
    * CI/CD: Scalability: Make all CI/CD related data to be stored on Object Storage
      * 10.5: [Traces on Object Storage](https://gitlab.com/gitlab-org/gitlab-ee/issues/4171)
      * 10.6: [Rake task for migrating traces](https://gitlab.com/gitlab-org/gitlab-ee/issues/4170)
      * 10.6: [Workhorse direct uploader for artifacts](https://gitlab.com/gitlab-org/gitlab-workhorse/merge_requests/238)
      * 10.6: [LFS direct upload](https://gitlab.com/gitlab-org/gitlab-ee/merge_requests/4794)
    * CI/CD: Resolve or schedule all AP1, SL1, SL2, bugs marked SP1 or SP2:
      * 10.5: (0 / 1) AP2, (1 / 3) SP1, (0 / 1) SP2, (1 / 2) SL2
      * 10.6: (0 / 1) AP1, (0 / 2) AP2, (1 / 1) SP1, (3 / 3) SP2, (0 / 2) SL2
    * CI/CD: Cost: Move all CI infrastructure to GCP
    * CI/CD: Quality: Test CI workflow with Runner by GitLab QA
      * 10.5: [End-to-end testing with GitLab QA](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/16619)
  * Monitoring
    * Monitoring: Bundle Alertmanager for proactive customer alerting notifications
      * 10.6 [In Review](https://gitlab.com/gitlab-org/omnibus-gitlab/merge_requests/2205)
      * 10.7 [TODO](https://gitlab.com/gitlab-org/gitlab-ee/issues/5158)
    * Monitoring: Prometheus deploy for customer apps feature
      * 10.5 [Done](https://gitlab.com/gitlab-org/gitlab-ce/issues/41053)
    * Monitoring: Ship 10 new alerts for monitoring GitLab
    * Monitoring: Instrument gitlab-shell
    * Monitoring: Ship 100% of committed deliverables issues each release
      * 10.4: (Bugs: 3/6, Features: 5/11) 53%
      * 10.5: (Bugs: 1/2, Features: 7/18) 40%
      * 10.6: (Bugs: 3/3, Features: 8/13) 69%
  * Security
    * Security: GDPR: Complete data breach notification policy and data mapping requirements (Compliance-KW)
    * Security: FIPS 140-2: Research requirements and provide guidance to development team to implement (Compliance-KW)
    * Security: Complete Remainder of 10 Risk Assessment Actions (Abuse-KW/JT/JR)
    * Security: Automate metrics for vulnerability initiatives: HackerOne, external & internal assessments (Automation-JT)
    * Security: Conduct 2 product application security reviews (AppSec-JR)
    * Security: Manage Advance Notification Program for security releases (SecOps-KW/JT/JR)
    * Security: All parts of security active (AppSec-JR, Automation-JT, SecOps-KW/JT/JR, Abuse-KW/JT/JR, Compliance-KW)
  * Database
    * Database: [Make it more difficult for database performance issues to occur](https://gitlab.com/gitlab-com/infrastructure/issues/3474)
    * Database: [Improve workflow / structure of the database team](https://gitlab.com/gitlab-com/infrastructure/issues/3475)
    * Database: [Improve database performance](https://gitlab.com/gitlab-com/infrastructure/issues/3476)
  * Gitaly
    * Gitaly: Deliver 100% of committed scope for GCP migration milestone [#2](https://gitlab.com/gitlab-com/migration/milestones/2) by Jan 15
    * Gitaly: Deliver 100% of committed scope for GCP migration milestone #3 by Feb 15
    * Gitaly: Deliver 100% of committed scope for GCP migration milestone #4 by Mar 15
    * Gitaly: All migration points complete to Ready-for-Testing state by 7 February
    * Gitaly: Release Gitaly v1.0 (all endpoints complete to Opt-Out state)
    * Gitaly: Defined roadmap for Gitaly v1.1, focused on Optimization and Performance
  * Quality
    * Quality: Complete the work to [make GitLab QA production-ready](https://gitlab.com/gitlab-org/gitlab-qa/issues/126) => Done.
    * Quality: [Define the architecture of and produce an end-to-end prototype for a self-service metrics generator](https://gitlab.com/gitlab-org/gitlab-insights/issues/2)
    * Quality: Define and schedule high-value issues for [improving the staging test environment](https://gitlab.com/gitlab-com/infrastructure/issues/3177)
    * Quality: [Write 2 GitLab QA tests related to creating and managing Issues](https://gitlab.com/groups/gitlab-org/-/epics/44)
    * Quality: [Write 2 GitLab QA tests related to CI/CD](https://gitlab.com/groups/gitlab-org/-/epics/45)
  * Edge
    * Edge: [Work with backend teams to move 100% of EE-specific **files** and 50% of EE-specific **lines of code** to the top-level `/ee` directory](https://gitlab.com/groups/gitlab-org/-/epics/27)
    * Edge: [Investigate how to extract EE-specific files/lines of code for JavaScript, CSS, and Grape API](https://gitlab.com/gitlab-org/gitlab-ee/issues/4643)
    * Edge: [Reduce average CE pipeline duration to 30 minutes](https://gitlab.com/gitlab-org/gitlab-ce/issues/41726)
    * Edge: Solve at least 1 [outstanding performance issues](https://gitlab.com/gitlab-org/gitlab-ce/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=performance&milestone_title=No+Milestone)
  * Frontend
    * Frontend: Write 200 unit tests to resolve test debt
    * Frontend: Crush 300 backlogged bugs
    * Frontend: Ship 100% of committed deliverables issues each release
      * 10.5: 81% shipped (30/37)
      * 10.6: 61% shipped (22/36)
    * Frontend: Make sure all [Frontend community contributions](https://gitlab.com/groups/gitlab-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Community%20Contribution&label_name[]=frontend) are merged, closed, labeled “awaiting feedback”, or taken over by us an in active development
    * Frontend: Close our main JS technical debt topics: Library updates, Global Code splitting and reduce our bundle size significantly per page
    * Frontend: Set up site speed docker container in our CI, running automated daily, and pushing stats to existing Grafana instance
  * Data and Analytics: Create the execution plan for the data enabled user journey.
  * CMO: Build trust of, and preference for GitLab among software developers.
    * CMO: Hire Director, DevRel.
    * MSD: Develop interactive content for Developer Survey results and promote results through digital/social channels.
    * MSD: Grow followers by 20% through proactive sharing of useful and interesting information across our social channels.
    * MSD: Grow number of opt-in subscribers to our newsletter by 20%.
    * PMM: Plan and execute IBM Think corporate event.
    * PMM: Plan and execute GTM for acquisitions and partner launches.
    * PMM: Generate a customer persona map and 3 customer persona profiles.
  * CMO: Generate more company and product awareness including increasing lead over [BitBucket in Google Trends](https://trends.google.com/trends/explore?q=bitbucket,gitlab).
      * MSD: Implement SEO/PPC program to drive increase in number of free trials by 20% compared to last quarter, increase number of contact sales requests by 22% compared to last quarter, increase amount of traffic to about.gitlab.com by 9% compared to last quarter.
      * CMO: PR - G1, G2, T1 announcements.
      * CMO: AR - conduct intro briefings with all key Gartner analysts to include reviewing new positioning.

### Objective 3: Great team

* CEO: Hire according to plan
* CEO: Great and diverse hires
  * Global hiring
  * Sourced recruiting 50% of applicants
  * Hired candidates, on average, from areas with a [Rent Index](https://about.gitlab.com/handbook/people-operations/global-compensation/#the-formula) of less than 0.7
* CEO: Keep the handbook up-to-date so we can scale further
  * Handbook first (no presentations about evergreen content)
  * Consolidate and standardize role descriptions
* VPE: Consolidate and standardize job descriptions
* VPE: Launch 2018 Q2 department OKRs before EOQ1 2018
* VPE: Set 2018 Q2 hiring plan before EOQ1 2018
* VPE: Implement issue taxonomy changes to improve prioritization
* VPE: Record an on-boarding video of how to do a local build and contribute to the GitLab handbook
* Backend: Deliver two iterations toward aligning backend teams with the DevOps lifecycle
* Support
  * Support: Define HA Expertise with 7 support engineers updating HA documentation as defined by Product.
  * Support: Define Kubernetes Expertise with 7 support engineers updating Kubernetes documentation as defined by Product.
* UX
  * UX: [Deprecate outdated UX Guide and replace with design.gitlab.com to communicate current UX standards and solutions across teams.](https://gitlab.com/groups/gitlab-org/-/epics/30)
  * UX: [Write 3 public blog posts about GitLab UX and visual design case studies, best practices, anecdotes, or events](https://gitlab.com/groups/gitlab-org/-/epics/33)
* Quality: Document the context and background of release process improvements in the Handbook / Quality page
* Frontend: Establish and shape our Frontend specific on-boarding
* Data and Analytics: Corporate dashboard in place for 100% of company metrics.
* Data and Analytics: Capability to analyze cost per lead/SAO/SQO and marketing campaign effectiveness.
* Controller: ASC 606 implemented for 2017 revenue recognition
* Billing Specialist: Add cash collection, application and compensation to job responsibilities.
* Controller: Close cycle reduced to 9 days.
* Accounting Manager: All accounting policies needed to be in place for audit are documented in the handbook.
* Legal: Add at least one country in which headcount can be grown at scale.
* VPE: Hire a Director of Engineering
* VPE: Hire a Director of Infrastructure
* VPE: Hire a Database Manager
* VPE: Hire a Production Engineer
* Distribution: Hire a Distribution Engineer
* Discussion: Hire two developers
* Quality: Hire an Engineering Manager
* Security: Hire 2 Security Engineer, SecOps
* Platform: Hire 2 developers
* CI/CD: Hire 2 developers
* CMO: Hire Director, Product Marketing
  * PMM: Hire to Product Marketing team plan
* CMO: Hire Director, Corporate Marketing
  * CMO: Hire to Corproate Marketing team plan
* CMO: Hire Director, DevRel
  * CMO: Hire to DevRel team plan
* MSD: Hire to SDR team plan
* MSD: Hire SMB Customer Advocates
* MSD: Hire Manager, Online Growth
  * MSD: Hire to Online Growth team plan
* MSD: Hire to Field Marketing team  plan
* CCO: Launch training for making employment decisions based on the GitLab Values.
* CCO: Ensure candidates are being interviewed for a fit to our Values as well as ability to do the job, through Manager Training and Follow-up by People Ops.
* CCO: Analyze and make recommendations based off of New Hire Survey and Pulse surveys which will drive future KRs. Have at least 3 areas to improve each quarter. Ideally, we will also have 3 areas to celebrate.
  * https://gitlab.com/gitlab-com/people-ops/Training/issues/8
  * https://gitlab.com/gitlab-com/people-ops/Training/issues/7
* CCO: Iterate on the Performance Review process with at least two changes initiated by March.
* CCO-TA: Iterate the hiring process to decrease process cycle-times, increase efficiency on screening candidates and provide a better candidate experience.
* CCO-TA: Re-vamp and enhance our jobs page to help attract diverse quality talent enhance our emploement brand and position ourselves as hi-tech company.
* CCO-TA: Establish level of effort metrics to ensure process efficencies to include: recruiter screened/hiring manager review ratio, Interview/Offer ratio, and Offer Accept ratio.
* CCO: Provide consistent training to managers on how to manage effectively. Success will mean that there are at least 15 live trainings a year in addition to curated online trainings.
* CCO: Align recruitings to Functional Groups with Focus on Low Rent Regions. At least 50% of GitLabbers should be hired from a Rent Index location that less than 0.7.
* CCO: Implement actionable Recruiting Metrics, including the ability to track an accurate source of hire for the majority of all hires.
* CCO: Target 2 Diversity recruiting Events/sources to attend and recruit from. Measure success to determine future plan.
* CCO: Increase Employee Referrals by 5%.
* CCO: Launch Harassment Prevention Training to all managers.
* CCO: Identify the right LMS for GitLab.
* CCO: Now that hiring managers have been trained on Reference Checking, beginning ensuring that Hiring Managers are verifying at least one reference per hire personally.
* CCO: Hiring at least one sourcer and one recruiter for EMEA/Central Asia.
* CCO: Prioritize the future countries for increased hiring based on pipeline, regulations, future sales, rent index. Begin steps to enable increased hiring outside the U.S.

## 2018-Q2

### CEO: Grow Incremental ACV according to plan. % of plan achieved, pipeline 3x minus in quarter, 70% at 70% of quota.

1. CMO: Build 3x minus in quarter pipeline for Q3. % of plan achieved.
1. CMO: Enough leads for sales. SCLAU of 10 per month for each SAL.
1. CRO: 120% of plan achieved.
1. CRO: Success Plans for all eligible customers.
1. CRO: Effective sales organization. 70% of salespeople are at 100% of quota.
2. Dir Channel: Increase Channel ASP by 50%
2. Dir Channel: Triple number of resellers above "Authorised Level"
2. Dir Channel: Implement VAR program (SHI, Insight, SoftwareOne, etc)

### CEO: Popular next generation product. Ship first iteration of complete DevOps, GitLab.com uptime, zero click cluster demo.

1. Product: Ship first iteration of [complete DevOps](https://about.gitlab.com/2017/10/11/from-dev-to-devops/).
1. VPE: GitLab.com ready for mission critical tasks. Move in April, 99.9% uptime, tested DR.
1. CFO: Make progress on having public clouds run us. 2 running everything.
1. Dir. Partnerships: Sign agreement to migrate target OS project
1. Dir. Partnerships: Strategic cloud partner chooses GitLab SCM for its single tenant offering
1. Dir. Partnerships: Successfully track and present data on the usage touch points for attribution tracking of our cloud agreement
1. VPE: Ship first iteration of [complete DevOps](https://about.gitlab.com/2017/10/11/from-dev-to-devops/).
1. CTO: Make sure cloud native installation, PaaS and cluster work well. Zero clicks.
1. CTO: Make sure we use our own features. 3 features.     

### CEO: Great team. Active recruiting for all vacancies, number of diverse per vacancy, real-time dashboard.

1. CCO: Active recruiting. 100% of vacancies.
1. CCO: Increase double diverse candidates (underrepresented and low rent index). Number per vacancy triples QoQ.
1. VPE: Hire great infrastructure leadership. Great director and manager hired in low rent location.
1. CFO: Real-time dashboard for everything in the Metrics sheet. 100% of metrics.

## Archive

* [2017-Q3](/okrs/2017-q3/)
* [2017-Q4](/okrs/2017-q4/)

[discussion-deliverables]: https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Discussion&label_name[]=backend&label_name[]=Deliverable
[discussion-sl1]: https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Discussion&label_name[]=backend&label_name[]=SL1
[discussion-sp1]: https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Discussion&label_name[]=backend&label_name[]=SP1
[discussion-ap1]: https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Discussion&label_name[]=backend&label_name[]=AP1
[discussion-backend-bug]: https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=Discussion&label_name%5B%5D=backend&label_name%5B%5D=bug&scope=all&sort=updated_desc&state=opened
[discussion-backend-community-contributions]: https://gitlab.com/groups/gitlab-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Community%20Contribution&label_name[]=Discussion
[discussion-10-5-update]: https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/9934
[discussion-10-6-update]: https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/10446
