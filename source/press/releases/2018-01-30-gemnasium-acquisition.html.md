---
layout: markdown_page
title: "GitLab acquires Gemnasium to accelerate its security roadmap"
---

### Developers can use GitLab to automatically detect security vulnerabilities, protecting code from attacks

**SAN FRANCISCO, CALIF.—January 30, 2018** – Today [GitLab](https://about.gitlab.com/), the leading integrated product for the entire DevOps lifecycle, announced it has acquired [Gemnasium](https://gemnasium.com/), a company that provides software to help developers mitigate security vulnerabilities in open source code. GitLab is acquiring both Gemnasium’s technology and its team of experts, who will come on board to implement robust security scanning functionality natively into GitLab’s CI/CD pipelines. Automating application security testing allows businesses to develop and iterate software faster without sacrificing a strong security posture.

With open source software (OSS) adoption rapidly growing, the risk for vulnerabilities is at an all-time high. The number of open source modules in use continues to increase, so the surface area for vulnerabilities to be exploited has expanded far and wide. As the dependency tree goes deeper, it can be daunting or even impossible for developers to keep track of which software they are using and what ramifications its use may have on the business.

Gemnasium has created the best-in-class solution for managing security threats related to open source dependencies. With a larger database and advanced algorithms, their solution finds more vulnerabilities with fewer false positives. This allows developers to protect their code and spot vulnerabilities in open source software before attackers can expose an organization to threats such as malware injections, Denial-of-Service (DoS) attacks and data breaches.

“As a team of engineers, we’re excited to join the GitLab team and bring the benefits of Gemnasium to the DevOps lifecycle,” said Philippe Lafoucrière, founder of Gemnasium. “Our goal has always been to help developers build the most secure software possible and joining GitLab allows us to do just that.”

GitLab has already begun adding native security functionality, such as the addition of Static Application Security Testing (SAST) in the [10.3 release](https://about.gitlab.com/2017/12/22/gitlab-10-3-released/), along with Dynamic Application Security Testing (DAST) and Container Scanning in the [10.4 release](https://about.gitlab.com/2018/01/22/gitlab-10-4-released/). With the Gemnasium team coming on board, GitLab will further accelerate its security roadmap to bring developers a native and seamless experience for rapidly deploying secure code.

“GitLab’s vision is to provide best-in-class tools for the complete DevOps lifecycle in a single application,” said Sid Sijbrandij, CEO of GitLab. “Gemnasium is the best dependency monitoring solution on the market, and we are excited to be making its team part of the GitLab experience.”

[Gemnasium.com](http://Gemnasium.com) will be winding down with an expected end-of-life date of May 15. The Gemnasium team encourages users to migrate to GitLab CI/CD or explore similar services like Snyk, SourceClear, WhiteSource, and BlackDuck. For more information, visit https://gemnasium.com/blog/gemnasium-is-acquired-by-gitlab/.

**About GitLab**
Since its incorporation in 2014, GitLab has quickly become the leading self-hosted Git repository management tool used by software development teams ranging from startups to global enterprise organizations. GitLab has since expanded its product offering to deliver an integrated source code management, code review, test/release automation, and application monitoring product that accelerates and simplifies the software development process. With one end-to-end software development product, GitLab helps teams eliminate unnecessary steps from their workflow, significantly reduce cycle time and focus exclusively on building great software. Today, more than 100,000 organizations, including Ticketmaster, ING, NASDAQ, Alibaba, Sony, and Intel; and millions of users, trust GitLab to bring their modern applications from planning to monitoring, reliably and repeatedly.

**Media Contact**

Nikki Plati

[gitlab@highwirepr.com](gitlab@highwirepr.com)

415-963-4174 ext. 39
