---
layout: job_page
title: "Senior Director, Data and Analytics"
---

## Responsibilities

* Ensure the Company’s cloud and on-premise data is centralized into a single data warehouse that can support data analysis requirements from all functional groups of the Company.  See our [BizOps effort](https://gitlab.com/gitlab-org/bizops).
* Create a common data framework so that all company data can be analyzed in a unified manner.
* Design, implement, and manage the organization's go-to-market systems archetecture, including the sales and marketing infrastructure accross Marketing, Sales, Customer Success and Accounting used to support the customer journey.
* Create and execute a plan to develop and mature our ablility to measure and optimize usage growth and our [user journey](https://about.gitlab.com/handbook/journeys/#user-journey).
* Ensure that all transactional systems can communicate with each other either directly or via the data warehouse and that production data adheres to a unified data model.
* Ensure that each metric in the Company’s dashboard has a single source of Truth and that data ownership and validation are incorporated on a consistent basis.
* Determine the level of integration necessary between transactional systems to deliver the right data in the right context to users.
* Develop a roadmap for system expansion, evaluate existing systems and ensure future systems are aligned with the Company’s data architecture plan which you will largely help develop.
* Implement a set of processes that that ensure any changes in transactional system architecture are documented and their impact on the company’s overall data integrity are considered prior to changes being made.
* Collaborate with all functions of the company to ensure data needs are addressed and system.
* This position reports directly to the CFO and works closely with the executive team to develop an organization plan that addresses company wide analytic resources in either a direct report or matrix model.

## Requirements

* Minimum 4 years hands on experience in a data analytics role
* Experience with a high growth company using on-premise tools and on-demand (SaaS) transactional systems
* Hands on experience with Python, MySQL, SQL, ETL tools.  Experience with Postgres is a plus.
* Have previously lead a corporate data platform project
* Experience with open source data warehouse tools
* Experience working with multiple executive level business stake holders
* Experience with analytic and data visualization tools such as Tableau, Looker, etc
* Experience with Salesforce, Zuora, Zendesk and Marketo
* Share and work in accordance with our values
* Must be able to work in alignment with Americas timezones
* Successful completion of a [background check](/handbook/people-operations/code-of-conduct/#background-checks).

## Hiring Process

Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/team).

* Selected candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a first interview with our CFO
* Candidates will then be invited to schedule a second interview with our CRO, CMO and Director of Demand Generation
* Finally, candidates may be asked to interview with our CEO

Additional details about our process can be found on our [hiring page](handbook/hiring).
