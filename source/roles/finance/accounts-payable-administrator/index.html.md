---
layout: job_page
title: "Accounts Payable Administrator"
---

## Responsibilities

- Process full-cycle accounts payable process including vendor and invoice management, approval and weekly disbursement activities.
- Process expense reports, credit card account reconciliations, and payable side bank reconciliation.
- Fulfill all duties, including journal entries, reconciliations, and other assigned tasks, in a timely manner to comply with the close calendar, - - checklists, and other due dates as assigned.

## Requirements

- Must have experience with Netsuite
- Proficient with Excel and Google Sheets
- International experience is a plus
- Self-starter with the ability to work remotely and independently and interact with various teams when needed.
- You share our [values](/handbook/values), and work in accordance with those values.
- Successful completion of a [background check](/handbook/people-operations/code-of-conduct/#background-checks).

## Hiring Process

Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](https://about.gitlab.com/team/).

- Selected candidates will be invited to schedule a 30 minute screening call with our Global Recruiters
- Next, candidates will be invited to schedule a 45 minute interview with our Controller
- Candidates will then be invited to schedule a 45 minute interview with our CFO
- Finally, candidates may be asked to interview with the CEO
- Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](https://about.gitlab.com/handbook/hiring/interviewing).
