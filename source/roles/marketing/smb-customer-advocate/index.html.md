---
layout: job_page
title: "SMB Customer Advocate"
---

## Responsibilities
- Experience managing an inbound request queue and assisting prospects and customers with online chat, email, and phone correspondence.
- Familiarity with the needs of software development and IT operations teams.
- Basic understanding of application lifecycle management including the technology commonly used in application lifecycle management.
- Basic understanding of DevOps practices, including the cultural shift it represents and the benefits to building and shipping software upon adopting those practices.
- Experience in a role that puts customer needs above all else.
- Product demo experience.
- Working knowledge of software development methodologies such as Waterfall, Agile, and Conversational Development.
- Comfortable with frequent client phone calls to explain hard-to-understand concepts and deployment options.

## Requirements
- Excellent spoken and written English
- You are obsessed with making customers happy. You know that the slightest trouble in getting started with a product can ruin customer happiness.
- Affinity with software and the software development process
- Passionate about technology and learning more about GitLab
- 3+ years experience in sales, marketing, or customer service
- Experience with CRM and email automation software is highly preferred
- An understanding of B2B software, Open Source software, and the developer product space is preferred
- Is your college degree in French foreign politics with a minor in interpretive dance but you’ve been hacking together websites since you were 12? Perfect. We understand that your college degree isn’t the only thing that prepares you as a potential job candidate.
- Be ready to learn how to use GitLab and Git
- Start part-time or full-time depending on situation
- You share our values, and work in accordance with those values

## Hiring Process
Avoid the confidence gap; you do not have to match all the listed requirements exactly to apply. Our hiring process is described in more detail in our [hiring handbook](/handbook/hiring).

Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/team).

* Qualified applicants receive a short questionnaire from our Global Recruiters
  1. Why do you want to work as an SMB Customer Advocate specifically for GitLab?
  2. What is your greatest professional accomplishment?
  3. What motivates you?
  4. How would you describe your knowledge of Developer Tools and/or Open Source Software?
  5. What do you see as the main difference between supporting a prospect through their buyer's journey versus managing a sales cycle?
  6. Tell me about the last time you helped a customer or prospective customer with a problem or challenge they had.
* Selected candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a first interview with Business Development Representative Lead
* Candidates will then be invited to schedule a second interview with Manager, Sales and Business Development
* Candidates will be invited to schedule a third interview with our Senior Director, Marketing & Sales Development
* Candidates will be invited to schedule a fourth interview with our Chief Marketing Officer
* Finally, our CEO may choose to conduct a final interview with the candidate
* Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring).

## Compensation

Your compensation will be predominantly base, with a bonus based on overall team performance in achieving IACV goals in the SMB market. See our [market segmentation](https://about.gitlab.com/handbook/sales/#market-segmentation) for detail on how the SMB market is defined at GitLab. Also see the [Sales Compensation Plan](https://about.gitlab.com/handbook/finance/sales-comp-plan/). The compensation calculator below is used only for base salary. 
