---
layout: job_page
title: "Vice President of Alliances"
---

GitLab is looking for a Vice President for Alliances to join us in the San Francisco Bay area. In this role, you will develop and implement a multi-year sales plan to rapidly grow revenue and build strategic partnerships with large customers. You’ll be tasked with continuously enhancing our  alliance and partnership organization, processes, and metrics. As a Vice President of Alliances, you will serve as a key adviser to GitLab’s executives, Directors, and the sales group with regards to GitLab’s overall global alliance strategy.

## Responsibilities

* Identify, establish and execute agreements with strategic alliance partners to contribute towards our 3-year topline revenue growth plan
* GitLab runs on the cloud environments of Amazon, Microsoft, Google, IBM, Alibaba, Huawei, Oracle, and Digital Ocean and many of their products are also part of GitLab
* A recent example of a product that has become part of GitLab is the [acquisition of Gemnasium](https://techcrunch.com/2018/01/30/gitlab-acquires-gemnasium-to-strengthen-its-security-services/)
* Manage efforts to research and identify alliance prospects and targets
* Develop, maintain, and manage the implementation of strategic alliance sales and go-to­ market plans
* Collaborate with Sales, Products, and Marketing teams to develop new product offerings within existing and new lines of business
* Responsible for identifying potential acquisition partnerships, conducting due diligence and executing on integration strategies
* Manage external contacts, expectations and be the key GitLab Ambassador for our partner relationships  
* Measure and report results of alliance sales plan execution and identify areas for improvement and act to implement positive change
* Design, develop and execute engagement plans with Alliance key stakeholder while tracking Key Performance Indicators (KPI’s)
* Coordinate and collaborate with cross-functional organizations within GitLab to include marketing, products, finance, legal, sales, engineering and people operations
* Manage each stage of the alliance lifecycle and monitor the ROl and risk for both partners

## Requirements
* 6-8 years of senior leadership (VP or C-level) in planning and closing partnership deals valued at over $50M with enterprise technology companies
* 8+ years of experience in the field of DevOps
* Experience working in a fast-pace, start-up (500 employees and under), building key partnerships during scaling and pre-IPO
* Repeated success in “cradle-to-grave” penetration of Enterprise companies, outlining plans to build opportunities for collaboration and continued success
* Experience migrating large open-source projects to new tools for their communities
* Demonstrated complete understanding of the company, product, and landscape to make informed long-term strategic business decisions
* Established network of executive level contacts within the public cloud space, DevOps and open-source communities. Proven ability to translate those to new partnerships and business opportunities.
* Demonstrated analytical and data led decision-making.
* Based in the city of San Francisco
