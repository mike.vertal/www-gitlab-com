---
layout: job_page
title: "Implementation Engineer"
---

Implementation Engineers provide on-site or remote deployment of GitLab technology and solutions as well as training. The Implementation Engineer will act as the technical representative leading the direct interaction with the customer’s personnel and project teams by rolling out best practices. This role helps the customer to faster benefit from the GitLab solution and realize business value. Meanwhile, Implementation Engineers also need to provide feedback to product management and engineering team on the actual field experience through customer engagements and implementations.

To learn more, see the [Implementation Engineer handbook](/handbook/customer-success/implementation-engineering)

### Responsibilities

- Install & configure GitLab solutions in customer environment as per Statement of Work (SOW)
- Provide technical training sessions remotely and/or on-site
- Develop and implement migration plan for customer VCS & data migration
- Assist GitLab customer support team to diagnose and troubleshoot support cases when necessary
- Develop & maintain custom scripts for integration or policy to align with custom requirements
- Create detailed documentation for implementation, guides and training.
- Support Sales on quoting PS and drafting SOW to respond PS requests
- Travel required - 40%
- Managing creation of new and maintaining of existing training content

### Requirements

- Deep knowledge of software development lifecycle and development pipeline
- Understanding of continuous integration, continuous deployment, ChatOps, and cloud native
- Above average knowledge of Unix and Unix based Operating Systems
  - Installation and operation of Linux operating systems and hardware investigation/manipulation commands
  - BASH/Shell scripting including systems and init.d startup scripts
  - Package management (RPM, etc. to add/remove/update packages)
  - Understanding of system log files/logging infrastructure
- B.Sc. in Computer Science or equivalent experience
- Programming/scripting experience & skill is required (Bash & Ruby)
- Project management experience & skills
- SCM admin and/or PS experience would be a plus
- Set up HA/DR, working with Containers and Schedulers (Kubernetes preferred) and also experience with AWS stack (EC2, ECS, RDS, ElastiCache)
- Experience with Ruby on Rails applications and Git

## Manager, Professional Services

GitLab is a hyper growth company searching for people who are intelligent, aggressive, and agile with strong skills in technology, sales, business, communication, and leadership. Desire to lead through change is a must.

The Manager of Professional Services role is a management position on the front-lines. This is a player/coach role where the individual is expected to be experienced in and have advanced insight to the GitLab platform. The individual will contribute to territory and account strategy as well as driving the execution directly and indirectly. The individual will need to be very comfortable giving and receiving positive and constructive feedback, as well as adapting to environmental change and retrospecting on successes and failures. The Professional Services Manager will work together with the other managers within the Customer Success organization to help execute on strategies and vision with the Director.

The Manager of Professional Services is a critical part of the Customer Success organization at GitLab. Made up of Implementation Engineers, the professional services team owns the customer journey from the point of sale onward, ensuring a customer realizes the full value of GitLab throughout their organization.

You will have the opportunity to help shape and execute a strategy to help the Services team build mindshare and broad use of the GitLab platform within enterprise customers. Coaching your team members to becoming the trusted advisors to their customers. The ideal candidate must be self-motivated with a proven track record in software/technology sales or consulting and management. You should also have a demonstrated ability to think strategically about business, products, and technical challenges. You will also be responsible in helping grow and maintain our enterprise-level customers.

### Responsibilities

- Work with the Customer Success Director to help establish and manage goals and responsibilities for Implementation Engineers
- Assist in development of thought leadership, event-specific, and customer-facing presentations
- Share hands-on technical preparation and presentation work for key accounts helping sell on the value of what GitLab professional services has to offer
- Ensure the Implementation Engineers exceeds corporate expectations in core knowledge, communication, and execution
- Define and maintain a high bar for team member expectations and enable the team to achieve it
- Challenge the team and yourself to continually learn and grow as trusted advisors to clients
- Monitor performance of team members and provide timely feedback and development assistance
- Create, review, and approve formal statements of work, change requests, and proposals
- Prepare weekly revenue forecast worksheet and create action plans to address issues
- Manage resource assignments and staffing levels, including recruitment as needed
- Identify and implement improvements to the processes and tools used as a seasoned with experience leading teams of project managers and consultants in support of external customers
- Work together with our sales organization to propose, scope, and price professional services Statements of Work, including managing a PS sales overlay team
- Bring Professional Services team together with Solutions Architects and Technical Account Managers to plan and execute internal projects, ensure that teams have appropriate training and manage resources to deliver GitLab service offerings
- Provide leadership and guidance to coach, motivate and lead services team members to their optimum performance levels and career development
- Ensure delivery model is focused on quality, cost effective delivery of services, and customer success outcomes
- Remains knowledgeable and up-to-date on GitLab releases
- Documents services provided to customers, including new code, techniques, and processes, in such a way as to make such services more efficient in the future and to add to the GitLab community
- Works with the Product Engineering and Support teams, to contribute documentation for GitLab
- Help build programs that the TAMs and IEs will execute to effectively grow our enterprise customers

### Requirements

All requirements for the Implementation Engineer role apply to the Manager, Professional Services role, as the management role will need to understand and participate with the day-to-day aspects of the Implementation Engineer role in addition to managing the team. Additional requirements include:

- Experienced in and with advanced insight to the GitLab platform
- Experienced in giving and received positive and constructive feedback
- Able to adapt to environmental change and retrospecting on success and failures
- Previous leadership experience is a plus
- Experienced in collaborating with other managers and executing strategies
- Proven track record in software/technology sales or consulting and management
- Demonstrated ability to think strategically about business, products, and technical challenges

## Hiring Process

Applicants can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/team).

- Qualified applicants for the individual contributor position will receive a short questionnaire
- Selected candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
- Next, candidates will be invited to schedule a first interview with our Director of Customer Success
- For an individual contributor role, candidates will be invited to schedule an interview with a Customer Success peer and may be asked to participate in a demo of a live install of GitLab
- Individual contributors may also be asked to meet with a member of our Support team
- For a manager role, candidates will be invited to schedule interviews with our Regional Sales Directors for North America
- Candidates will be invited to schedule a third interview with our CRO
- Finally, candidates may be asked to interview with the CEO

Additional details about our process can be found on our [hiring page](https://about.gitlab.com/handbook/hiring/interviewing/).
