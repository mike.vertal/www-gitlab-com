---
layout: markdown_page
title: "Content"
---

## On this page
{:.no_toc}

- TOC
{:toc}


## Content Marketing

The content marketing organization includes social marketing and oversees the
GitLab [blog](https://about.gitlab.com/handbook/marketing/blog/).

## Editorial Mission Statement

Empower and inspire DevOps teams to collaborate better, be more productive, and ship faster by sharing insightful and actionable information, advice, and resources.

### Vision

Build the largest and most diverse community of cutting edge co-conspirators who are leading the way to define and create the next generation of software development practices.

### 2018 Core content strategy statement<a name="strategystatement"></a>

The content we produce helps increase awareness of GitLab’s complete and single application with the goal of broadening our market share and increasing sales by providing informative and persuasive content that makes DevOps teams feel excited, curious, and confident so that they can adopt & integrate industry best practices into their workflow.

## Communication

### Band
- **Erica Lindberg, Content Marketing Manager** [@erica](https://gitlab.com/erica)
  - Area of focus: content and campaign strategy and oversees the blog, case studies, social, web, and webcast programs. 
- **Rebecca Dodd, Content Editor** [@rebecca](https://gitlab.com/rebecca)
  - Area of focus: blog, newsletters, editorial calendar, and copyediting
- **Emily von Hoffman, Associate Social Marketing Manager** [@evhoffmann](https://gitlab.com/evhoffmann)
  - Area of focus: social marketing, live streams, and user generated content (UGC)
- **Aricka Flowers, Content Marketing Associate** [@atflowers](https://gitlab.com/atflowers)
  - Area of focus: case studies, email, web, and webcasts 
- **Suri Patel, Content Marketing Associate** [@suripatel](https://gitlab.com/suripatel)
  - Area of focus: case studies, email, web, and webcasts

### Channels
- **Chat:** please use the following Slack channels `#content` for general inquiries, `#blog` for questions regarding blog content, and `#twitter` for questions about social.
- **Issue trackers:**
  - [General Content Marketing](https://gitlab.com/gitlab-com/marketing/boards/104871)
  - [Blog posts](https://gitlab.com/gitlab-com/blog-posts/boards/409030)
  - [Webcasts](https://gitlab.com/gitlab-com/marketing/boards/356113)
  - [Case studies](https://gitlab.com/gitlab-com/marketing/boards/361043) - jointly owned with Product Marketing
  - [Q4 OKRs](https://gitlab.com/gitlab-com/marketing/boards/377172?scope=all&utf8=%E2%9C%93&state=opened&milestone_title=Q4%202017&label_name[]=Content%20Marketing)

## Defining "Content"

“Content” is an incredibly broad and all-encompassing term. In its most simplistic form, content means something that has substance. Because “content” does not carry a universal definition, it’s important to clarify its meaning in the context of what it means for GitLab.

At GitLab, we define content in two ways:

1. **Foundational content:** Substance or material dealt with in speech and editorial distinct from its form or style. Foundational content is the basis for all company communications and consists of three pillars: organization, audience, and product. 
2. **Published content:** Information made publicly available digitally or in print for a defined audience. Published content is what we share to educate and engage our community, users, prospects, and customers.

We can further define content by function.

### Content Marketing and Strategy<a name="contentmarketing"></a>

**The marketing technique of creating and distributing targeted, relevant, and valuable content** to attract, acquire, and engage a clearly defined and understood target audience. The objective is to drive and enable profitable customer action.

The role of the Content Marketing and Strategy function is to determine the content strategy to build and grow targeted audiences through published content, and assist in the planning and execution of marketing campaigns.

**This is currently the only operating content function at GitLab**

### Content Operations<a name="contentops"></a>

**The management and governance of content.** It includes aligning content to business goals, analysis and influences the development, production, evaluation, and sunsetting of content. The objective is to manage content as a strategic business asset.

The role of the content operations function is to categorize, audit, and analyze content to ensure that content is being utilized to its maximum potential and to perform gap analysis to identify areas where there is a lack of useful content.

### Checklist for good content<a name="checklist"></a>

- Relevant
- Useful
- User or customer-centered
- Clear, Consistent, Concise
- Supported

## Key Responsibilities

At the highest level, Content Marketing is responsible for building awareness, trust,
and preference for the GitLab brand with developers, engineers, and IT professionals.

1. Audience development
2. Editorial voice and style
3. Defining and executing the quarterly content strategy
4. Lead generation

## Campaigns<a name="campaigns"></a>
Leadgen is responsible for executing marketing campaigns for GitLab. We define a campaign as any programmed interaction with a user, customer, or prospect.  For each campaign, we will create a campaign brief that outlines the overall strategy, goals, and plans for the campaign.

## Campaign Brief Process<a name="campaignbrief"></a>
To create a campaign brief, first start with the "campaign brief template" (which can be found by searching on the google drive). Fill out all fields in the brief as completely as possible. Certain fields might not be applicable to a particular campaign. For example, an email nurture campaign leveraging text based emails won’t have a visual design component. This field can be left blank in that example.

Once the campaign brief is filled out, create an issue in the GitLab Marketing project and link to the campaign brief.

On the GitLab issue, make sure to:
* Tag all stakeholders
* Use the Marketing Campaign label
* Set the appropriate due date (the due date should be the campaign launch date)
* If there are specific deliverables, create a todo list in the issue description for each stakeholder along with a due date

## Customer case studies program

The goal of the customer case studies program is to share and market the success of our customers. We do this by helping
them craft their story in a way that makes them proud and excited to talk about their success with GitLab. 

### Objectives:
- Illustrate successful and measurable improvements in the customer’s business
- Show other users how customers are using GitLab
- Align content so that GitLab solutions reflect the challenges our customers, prospects, and the market requirements
- Develop and grow organizational relationships: speak at events on our behalf, promote their business
- Support our value proposition - Faster from planning to monitoring

A formal case study produces a polished, finished document published on our 
[customers page](https://about.gitlab.com/customers/). Case studies can include multiple formats, including
a web page, video, downloadable PDF, and associated blog posts. 

A customer story is a blog post about how a customer solved a particular problem or 
received a specific benefit and are published on GitLab's [blog](https://about.gitlab.com/blog/). 
Customer stories can also be anonymous. e.g. "A large international financial services 
company used Gitlab CI/CD to reduce build times from 1 hour to 10 mins."

## Customer case study process

**Account Executive:**

1. Ask the customer to agree to a formal case study and commit to a format: video testimonial, written case study, or both
2. Connect the customer with the content team to build out the story, potential deliverables, and timelines. 
3. [Create & fill out a new issue in the Marketing issue tracker](https://gitlab.com/gitlab-com/marketing/issues/new?issue) and use the "Customer_Story_Kickoff" template.

**Content team:**
1. Schedule a discovery call with the customer & product marketnig manager to uncover use cases and identify metrics. 
2. Confirm deliverables and set timelines and expectations with the customer. 
3. Identify & confirm case study storyline & schedule follow-up customer interview if necessary. 


**Explaining the Case Study process to a Customer & Creating the Case Study**:

Below is an example email that PMM may send customers to after the AE has introduced them to the customer. The email below will help the customer to get a better sense of what we are asking of them.

>Hi X,

>It's nice to e-meet you, we'd love to hear more about your journey to GitLab and potentially write a customer story on COMPANY NAME.

>Here are the steps that we'd work with you on.
>- 20-30 minute phone call to hear more about your industry, business, and team. (In the call, we would also like to hear more about your decision making process to first choose GitLab and then eventually purchase EE.)
>- Review of the draft customer story
>- Final review of the customer story Then once the customer story is agreed upon by you or someone on your team we will publish it on GitLab's channels.

>Please let me know if you're open to kicking off the customer story process on X date

### Collecting Metrics:
Possible quantitative metrics that the customer can be asked to share with GitLab include:
- Reduced cycle time
- Number of deploys in a given time frame
- Reduced number of bugs or Reverts
- Reduced number of admin hours
- Cost savings % through purchasing GitLab: Reduce the cost of managing a number of different people, projects, and platforms.
- Reduction in internal support tickets requests: Reduction in the number of support tickets team submitting to fix challenges, compared to initial SCM tool

The customer case study should then be written by Product Marketing team, and the draft sent to the customer for their input, and approval.

Other sections in the case study can be found on the customer's website or by searching the web - Sidebar summary, The Customer.

Following approval from the customer, the Design team should be sent a doc of the case study to include in the case study design template. The case study can then be published on our website.

### Case Study Format:

*Headline:* The name of the customer and the benefit they gained from implementing our solution

*Sidebar Summary:* Summarize key points
- Customer Name and Logo
- Customer Details: Country, Website
- Organization Type - Public/Private & Industry
- Annual Revenue - If publicly available
- Employees
- Summary of Key Benefits

*The Customer:* A brief introduction of the customer, not the solution.

*The Challenge:* What was the customer trying to change or improve. What regulations or market conditions drove the customer to search for a new solution. Share the steps the customer took to solve the problem, including other products and services they investigated.

*The Solution:* Detail the solution and how it aligns to the customers requirements. Also detail other solutions that GitLab interfaces with. Also include customer quote.

*The Results:* Detail how GitLab supported the customer to solve their problems. This is where the use of benchmarking metrics such as such as time saved, reduced costs, increased performance etc. are required.

*About GitLab:* Short paragraph on GitLab - about, solutions etc. Call to action of solutions offered.

*Possible Additional Supporting Documents:*
- Customer Deal Summary – High Level PowerPoint summary after deal first signed.
- Customer Success Overview
- Infographic – Single page A4 summary with diagrams and measurable benchmarks
- Benchmark Metrics
- Publish on website
- Video - Short video summary if customer is willing to participate - Perforce example
- Blog Post - Blog post to launch customer case study
- Keywords for SEO etc.

Case studies should put the spotlight on our customers and tell a story about the ways in which they help their own customers. In telling that story, we should detail how GitLab helps them get that job done. Case studies should include metrics, but a compelling story can be told without them if there aren't any available.

### SLA

Case studies should always take high priority. Once a case study is initiated,
it should take **no longer than 2 weeks** to turn around a draft for review. Once the case study has been reviewed and approved by the customer,
it should take **no longer than 5 working days to publish on the marketing site.**

### Case Study Process

1. Once the customer has agreed to a case study, Content marketing manager will assign to a writer and label `CMM - Planning`. Deadline for completion is 2 weeks from the date the issue is was labeled `CMM - Planning`. Add the deadline to the issue.
3. Writer will submit audio recording for transcription using [Transcription Panda's](https://transcriptionpanda.com/) `standard` (2-3 day turnaround) option.
4. Once transcription is returned, writer will create a draft following the [case study format](https://about.gitlab.com/handbook/marketing/product-marketing/#case-study-format)
5. Once the draft is completed, writer will label `CMM - In review` and ping the PMM to review and send for approval.
6. Once the case study has been reviewed and approved by the customer, follow the publishing process below to add to the marketing site.

### Publishing

1. Start by creating a `.yml` file in `/data/case_studies` directory under Marketing site repo (www-gitlab-com project).
2. Keep the name of file same as company name (this is not mandatory but it is easier to manage), for eg; if company name is "Foobar", create a file as `/data/case_studies/foobar.yml`.
3. Once created, add contents of the file using [this sample Case Study template](https://gitlab.com/gitlab-com/www-gitlab-com/snippets/1685439), and then update the values of each property based on case study details, remember, **do NOT change property names**.
4. Once this file is added, you'll need to restart marketing site server by first closing it using `Ctrl+C` and then running `bundle exec middleman` again.
5. When the server is started, you can view generated Case Study page in your local instance of marketing site under the URL, for eg; `http://localhost:4567/customers/foobar`.
6. Please note that you don't need to restart server again if you make any changes to `foobar.yml` file, this is required only when a **new** file is added to the `/data/case_studies` directory.

## Giveaways <a name="giveaways"></a>

We use giveaways to encourage and thank our community for participating in marketing events such as surveys, user-generate-content campaigns, social programs, and more.

### Giveaways Process <a name="giveawaysprocess"></a>

**Pre-giveaway**
1. Create an issue and tag Emily von Hoffmann (@evhoffmann) to determine the
rules of engagement and Emily Kyle (@emily) for prizes.
2. Create and publish an [Official Sweepstakes Rules page](#officialrules)

**Post-giveaway**
1. Winners must sign an Affidavit of Eligibility & Liability, Indemnity, and Publicity Release. Use the "Affidavit of Eligibility - Sweepstakes" template found on the google drive.
2. Announce the winners

### Social Support & Logistics for Giveaways

#### Creating the Campaign

- Set a launch date
- Ask for social image(s) with text (if organic posts only) explaining the offer/ask
- Set an initial deadline for submissions, so you can have multiple pushes at interval & ramp up energy  
- Finalize the delivery method: form vs. tweets vs. retweets, depending on the goals of the campaign
    - Pros of a form: Neat, uniform, easy for us to keep track of, no downsides of low engagement (i.e., responses not visible)
    - Pros of asking for submissions via Twitter: we could more easily RT cool responses, get more out of a hashtag, etc.
    - Pros of asking for RTs in exchange for swag: very little backend to do on social afterwards, except to announce the winners of swag
- Finalize the ask, making sure it's extremely clear what you want to happen (`Share your GitLab story!` `Tell us your favorite thing you made with GitLab` `tell us a time GitLab helped you out of a tight spot`)
    - Make sure the ask can be intuitively communicated via whichever delivery method you're using, i.e., the tweet doesn't need to explain everything if you're pointing to a form or blog post. If you're not pointing to anything, make sure the tweet plus possible image text must make sense by themselves. Use threads for more space!

#### Pre-launch

- Finalize the timeline for when the reminders/follow-ups will go out, add to social schedule and leave some space around them to RT/engage with responses
- Finalize copy for all pushes
- If swag is involved, create a google sheet with swag codes from Emily Kyle
- Finalize hashtag
- Ask community advocates to review all copy (tweets, form, blog post) and adjust according to their suggestions
- Make sure the community advocates are aware of the campaign timeline/day-of
- Designate a social point person to be "on duty" for the day-of and one person who can serve as backup
- Let the broader GitLab team know that the social campaign is upcoming and ask for their support

#### Day of giveaway
- If you have entries for the giveaway in a spreadsheet, use [random.org](https://www.random.org/) to generate a random number. Match the number to the corresponding row in your spreadsheet to identify the winner. **Never enter email addresses or personal information of participants into a third-party site or system we do not control.**
- Try to schedule first push or ask a team member to tweet the first announcement early (ex: around 4 am PT) to try to have some overlap with all our timezones
- If you're asking for RTs in exchange for swag, make sure there's a clearly communicated cut-off to indicate that the giveaway will not stretch into perpetuity. One day-long is probably the longest you want a giveaway to stretch, or you can limit to number of items.
- Plan to engage live with people
   - If your promise was to give away one hoodie per 25 RTs, do it promptly after that milestone is crossed. It adds to the excitement and will get more people involved
- Announce each giveaway and use handles whenever possible, tell them to check their DMs
- DM the swag codes or whatever the item is
- In your copy, directly address the person/people like you are chatting with them irl
- RT and use gifs with abandon but also judgment

#### After the Giveaway
- Thank everyone promptly, internal & external
- Write in the logistics issue of any snags that came up or anything that could've gone better
- Amend hb as necessary for next time

### How to Create an Official Sweepstakes Rules Page <a name="officialrules"></a>

1. Create a new folder in `/source/sweepstakes/` in the www-gitlab-com project. Name the folder the same as the giveaway `/source/sweepstakes/name-of-giveaway`
2. Add an index.html.md file to the `/name-of-giveaway/` folder
3. Add the content of [this template](https://gitlab.com/gitlab-com/marketing/blob/0252a95b6b3b5cd87d537dabf3d1675023f1d07d/sweepstakes-official-rules-template.md) to the `index.html.md` file.
4. Replace all bold text with relevant information.
5. Create merge request and publish.

## Webcasts<a name="webcasts"></a>

Webcasts are one the greatest tools we have to communicate with our audience. GitLab webcasts aim to be informative, actionable, and interactive.

### Best Practices

1. Give yourself **at least** 15 days of promotion.
2. Send invitation emails 3 weeks out, 2 weeks out, 1 week out, and 2 hours before event.
3. Only send promotional emails Tuesday, Wednesday, or Thursday for optimal results.
4. Send reminder emails to registrants 1 week out, day before, and one hour before the event.
5. Host webcasts on a Wednesday or Thursday.
6. Post links to additional, related resources during the event.
7. Include "contact us" information at the end of the presentation.
8. Send the recording to all registrants, whether they attended or not.
9. Publish a post-webcast blog post capturing some key insights to encourage on-demand registrations.
10. Add webcast to the GitLab resources page.

### Speaker Approval

The Lead Generation team depends on GitLab's subject matter experts to deliver webcast presentations. However, we must ensure that when we ask a speaker to participate on a webcast that the work is approved. Please use the following process when asking someone outside of marketing
to participate on a webcast.

1. Have an abstract of the content prepared before asking for a presenter.
2. Send the abstract to both the proposed speaker and their manager to review. **A speaker is not considered booked unless they have approval from their manager.**
3. Address and resolve any concerns regarding the abstract.
4. Once the manager approves and the speaker accepts, you can move forward with the webcast.

### Tips for Speakers

Here are some basic tips to help ensure that you have a good experience preparing for and presenting on a webcast.

#### Before Committing
Ask us any questions you have about the time commitment etc. and what exactly our expectations are. Talk about it with your manager if you're on the fence about your availability, bandwidth, or interest. Make sure you're both on the same page. We want this to be a meaningful professional development exercise for you, not a favor to us that you're lukewarm about — if you feel that way, none of will be able to do our best job. We'll be honest with you, so please do the same for us.

#### Before the Dry Run
Select and set up your presentation space. Pick a spot with good wifi, and we recommend setting up an external mic for better audio quality, although this is optional. If you will be presenting from your home, alert your spouse/roommates of the time/date & ask them to be out of the house if necessary. Depending on your preferences and comfort level with public speaking, run through the script several times.

#### Before the Presentation
Try to get a good sleep the night before, and, if the presentation is in the morning, wake up early enough to run through your notes at least once. Review our [Positioning FAQ](https://about.gitlab.com/handbook/positioning-faq/), or keep the page handy in case you are asked in the Q&A about how GitLab compares to our competitors.

### Logistical Set-up

1. Create webcast in Zoom
   - Make sure the registration confirmation email and the reminder emails are set to send from Zoom. As Zoom implements their more robust Marketo integration, we will send from Marketo, but for now we need to send from Zoom to ensure that the correct unique link is sent to registrants.
1. Create the webcast program in Marketo
   - Title the webcast in the following format: `YYYYMMDD_{Webcast Title}`. For example, 20170418_MovingToGit
1. Update `My Tokens` at the webcast program level
   - DO NOT UPDATE THE `apiKey` or `apiSecret`. These should be the same for every webcast
   - Update the `{{my.zoomWebinarId}}` token with the webinar ID from the Zoom webcast created in step 1
   - Update the add to calendar tokens
      - Copy the Google Calendar link from the webinar page on Zoom
      - Update the information in the iCal and Outlook calendar files (these will be identical)
   - Update the event date and time
   - Update the email body with the description of the webcast
1. Turn on smart campaigns in Marketo
   - Activate the `Zoom Attended` campaign
   - Activate the `Webcast_Registered` campaign
   - Activate the `Registered From Zoom` campaign
   - Schedule the `Zoom Absent Attendees` campaign to run after the webcast will be finished (give the campaign a few hour buffer minimum)
1. Set up Marketo integration within Zoom
   - Log in to Zoom, and select the webcast from the `Webinars` tab
   - Scroll down and click on the `Integration` tab
   - Click `Edit` next to `Generate Leads in Marketo`
   - Turn on `Send registration information to a Smart Campaign` and select the `Registered From Zoom` smart campaign for the proper webcast program
   - Turn on `Send attendee information to a Smart Campaign` and select the `Zoom Attended` smart campaign for the proper webcast program
   - Select the `ZoomWebinarOtherInfo` custom object, check the following boxes, and select the corresponding Marketo Custom Object Fields:
      - Webinar ID
      - Webinar Topic
      - Q&A
      - Poll
1. Edit the landing page to have the appropriate webcast description, date, and time.

### EEP Demo Webcast

We host an [EEP demo webcast](https://about.gitlab.com/eep-demo/) on the second and fourth Wednesday of every month in collaboration with the Solution Architects. This webcast is intended to give an overview of the workflow and tools available in our Enterprise Edition Premium offering.

#### Rotation

As new content marketers and solution architects come onboard, add them to the end of the rotation.

**Project managers**
- Erica L
- Emily vH
- Rebecca

**Solution Architects**:
- Reb
- John W.
- David T.
- Victor H.
- Joel K.
- Chris M.
- Brendan O.
- Jesse L.

### Going Live on YouTube

We're happy to support teams who want to conduct meetings, brainstorms, demos etc. [live on YouTube](/handbook/tools-and-tips/#sts=Livestreaming to YouTube). This is a great way to practice our value of transparency and involve the community in work that often goes on "behind the scenes." A regular example of this is our live streamed kickoffs and retrospectives for each release.

## Social Channels and Audience Segmentation

### Twitter

**Content/Execution**
* Broad audience - all our content gets shared here, but tone should still appeal to devs.
* 5 or more tweets per day, plus retweets
  * Schedule between 7 am - 10 pm ET (to have some overlap with both EMEA and PT)
  * At least 30 mins apart
* Aim for variety & roughly even mix of main topics: Git, CI/CD, collaboration, DevOps, employer branding, product-specific, community articles & appreciation
* Check mentions at least once per day, pull out articles to share with the team and favorite positive mentions.
*  Keep a list of our most engaged followers; keep these in mind for opportunities outside of social--blogging, live streamed interview etc.

**Goals**
* 15 tweets per day
* 2-4 videos published natively per month
* 5 lead-generating content items from the backlog published per month
* Consider [takeovers.](http://www.telegraph.co.uk/news/2017/07/12/work-experience-boy-takes-southern-rails-twitter-account-things/) Would need to develop an instructions kit & think about how/who to pilot.

### Facebook

**Content/Execution**
- Developer/community- and thought-leadership focused
- 1-2 facebook posts per day
- At least 2 hours apart
- Live events are more company/culture focused
    - talks by or AMAs with People Ops, etc.

**Goals**
- Consider streaming a webcast on Facebook Live (a simple toggle switch in Zoom allows this) and compare the performance to YouTube live streaming
- When the speaker cameras are enabled in Zoom webinars, natively publish clips of webcasts for post-promotion.

### LinkedIn

**Content/Execution**
- IT buyer/manager focus
- 3-5 posts per week
  - Non-tutorial original blog posts
  - Press releases/product announcements
- Posts around particular features, releases, company culture, people ops new practices, problem/solution or personal growth structured posts; include links back to homepage

**Goals**
* Identify key LinkedIn groups to join/share content in
* Native posts on LinkedIn
   * Encourage targeted employees to do so; think of incentive/framing as professional development that we will help edit.
* Curate and share articles from other publishers on remote work/culture that we want to elevate

### Medium

**Content/Execution**
* Developer/community- and thought leadership-focused
* Favorite articles about GitLab
* Aim to post one article to Medium every two weeks
* Cross-post our original thought-leadership posts to Medium, linking back to blog
  - Use the [import tool](https://help.medium.com/hc/en-us/articles/214550207-Import-post); do not copy and paste.

### YouTube

**Content/Execution**
- Developer/community-focused
- Live events are more tech focused
  - FGUs, pick your brain meetings, demos, brainstorms, kickoffs

### Google+

**Content/Execution**
- Re-post older articles from the archive periodically that we want to continue giving a boost
- Aim for at least one post per week

## Event Promotion

#### After Committing to an Event

- Find relevant handles (of speakers/moderators/hosts) or hashtags (if part of bigger conference for example), or create our own; try to include across all tweets for that event, characters permitting
- Find event-related images to attach to all tweets (event logo showing GitLab sponsorship; speaker team pic; GitLab logo; or any sharable content designed for that event)
- Schedule a tweet asap announcing our participation

#### Leading up to an Event, Schedule Tweets to go out:

- 1 week before event (counting down, include registration link if our event, include speaker team pic or other event branding where applicable)
- 1 day before event (same as above)
- During event referencing something in the content of the presentation if applicable/ not redundant (x2 or more depending on length of event and amount of content)
- Need to find balance between the dev advocates promoting themselves at events and when they are speaking- maybe one of these tweets is us retweeting them?

#### While at the Event:
- Take pictures of booth and/or team members at booth
- Try to tweet them out ~1 hour or more before event (mention our location in the venue if applicable/useful)

#### After the Event:
- Thank those who attended or stopped by our booth in tweet with photo from the event

## Ensuring your Post Will Have a Functional Card and Image

When you post a link on Facebook or Twitter, either you can see only a link, or a full interactive card, which displays information about that link: title, **description**, **image** and URL.

For Facebook these cards are configured via [OpenGraph Meta Tags][OG]. Twitter Cards were recently setup for our website as well.

Please compare the following images illustrating post's tweets.

A complete card will look like this:

![Twitter Card example - complete][twitter-card-comp]

An incomplete card will look like this:

![Twitter Card example - incomplete][twitter-card-incomp]

Note that the [first post] has a **specific description** and the image is a **screenshot** of the post's cover image, taken from the [Blog landing page][blog]. This screenshot can be taken locally when previewing the site at `localhost:4567/blog/`.

### Defining Social Media Sharing Information

Social Media Sharing info is set by the post or page frontmatter, by adding two variables:

```yaml
description: "short description of the post or page"
twitter_image: '/images/tweets/image.png'
```

This information is valid for the entire website, including all the webpages for about.GitLab.com, handbook, and blog posts.

#### Images

All the images or screenshots for `twitter_image` should be pushed to the [www-gitlab-com] project at `/source/images/tweets/` and must be named after the page's file name.

For the second post above, note that the tweet image is the blog post cover image itself, not the screenshot. Also, there's no `description` provided in the frontmatter, so our Twitter Cards and Facebook's post will present the _fall back description_, which is the same for all about.GitLab.com.

For the handbook, make sure to name it so that it's obvious to which handbook it refers. For example, for the Marketing Handbook, the image file name is `handbook-marketing.png`. For the Team Handbook, the image is called `handbook-gitlab.png`. For Support, it would be named `handbook-support.png`, and so on.

#### Description

The `description` meta tag [is important][description-tag]
for SEO, also is part of [Facebook Sharing][og] and [Twitter Cards]. We set it up in the
[post or page frontmatter](../blog/#frontmatter), as a small summary of what the post is about.

The description is not meant to repeat the post or page title, use your creativity to describe the content of the post or page.
Try to use about 70 to 100 chars in one sentence.

As soon as you add both description and social sharing image to a page or post, you must check and preview them with the [Twitter Card Validator]. You can also verify how it looks on the FB feed with the [Facebook Debugger].

#### Examples

To see it working, you can either share the page on Twitter or Facebook, or just test it with the [Twitter Card Validator].

- Complete post, with `description` and `twitter_image` defined:
[GitLab Master Plan](/2016/09/13/gitlab-master-plan/)
- Incomplete post, with only the `description` defined:
[Y Combinator Post](/2016/09/30/gitlabs-application-for-y-combinator-winter-2015/)
- Incomplete post, with none defined: [8.9 Release](/2016/06/22/gitlab-8-9-released/)
- Page with both defined: [Marketing Handbook](/handbook/marketing/)
- Page with only `twitter_image` defined: [Team Handbook](/handbook/)
- Page with none defined: [Blog landing page](/blog/)


<!-- Social Marketing Handbook - ref https://gitlab.com/gitlab-com/marketing/issues/524 -->

<!-- identifiers, in alphabetical order -->

[blog]: /blog/
[description-tag]: http://www.wordstream.com/meta-tags
[facebook debugger]: https://developers.facebook.com/tools/debug/
[first post]: /2016/07/19/markdown-kramdown-tips-and-tricks/
[OG]: https://developers.facebook.com/docs/sharing/webmasters#markup
[second post]: /2016/07/20/gitlab-is-open-core-github-is-closed-source/
[twitter card validator]: https://cards-dev.twitter.com/validator
[twitter cards]: https://dev.twitter.com/cards/overview
[twitter-card-comp]: /images/handbook/marketing/twitter-card-complete.jpg
[twitter-card-incomp]: /images/handbook/marketing/twitter-card-incomplete.jpg
[www-gitlab-com]: https://gitlab.com/gitlab-com/www-gitlab-com
