---
layout: markdown_page
title: "Collaboration & Effective Listening"
---
## On this page
{:.no_toc}

- TOC
{:toc}



## Collaboration

**What is collaboration?**

Collaborate, according the the Merriam- Webster Dictionary, means “to work jointly with others or together especially in an intellectual endeavor.”

A workforce, also according to the Merriam-Webster Dictionary, is comprised of “the people engaged in a specific activity or enterprise.”

Put it together and a collaborative workforce is a group of people working together toward a specific goal.  

## Communication: Speak Clearly & Listen Openly

Think about a time that you tried to share information on how to complete a task with someone and although the task was completed, it was not completed to your satisfaction, what happened and what could you have done?

**What is communication?**

- Communication is the exchange of ideas from one to another.

**What is effective communication?**

- Effective communication depends on clarity, speech patterns, and the intonation conveyed by the sender of the message. Effective communication is also the ability of the listener to attend to the message.

-  It is estimated that people (listener) will filter out or change the intended meaning of what is heard in 70% of all communications. [source](https://1personalcareercoach.com/art-listening-good-leader)

**Myths about Listening**

- Everybody knows how to listen
- Sending messages is more important than receiving them
- Listening is easy and passive
- Hearing and listening are the same thing
- An effective speaker commands audience attention
- Communication is the sender’s responsibility
- Communication is the sender’s responsibility
- Listening is done with our ears
- Listening skills are practiced not learned
- Listening ability comes from maturity


**Tips for Effective Listening**

- Concentrate
- Give unequivocal attention to the speaker
- Don’t anticipate what the speaker means
- Test the message not the messenger
- Respect cultural difference and boundaries
- Develop the fine art of empathy
- Try not to interrupt
- Focus on feelings and not grammar or vocabulary
- Silence is the golden rule

## THINK
