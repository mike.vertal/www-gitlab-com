---
layout: markdown_page
title: "360 Feedback"
---
## On this page
{:.no_toc}

- TOC
{:toc}


### 360 Feedback

GitLab will be releasing the next round of written performance feedback on March 1, 2018 using Lattice. This time there will be 360 feedback where managers and direct reports will give feedback to each other, and each team member will add a certain number of peers to provide feedback as well. There will not be ratings associated with the feedback.

**Q: What is the timeline for the March 2018 360 Feedback?**

**A:**

* Cycle opens: March 1, 2018
* Peer feedback selections made by: March 9, 2018
* Feedback forms due/review cycle closes: April 9, 2018
* Meeting with direct reports: Scheduled by April 13, 2018

**Q: How many peers should I nominate and who?**

**A:** There is no limit to the number of peers you can select for peer review, but we recommend somewhere between three to seven. Anyone at the company should be considered a peer reviewer for anyone else at the company. Seek feedback from those you feel have the most meaningful feedback to give and have worked most closely with you. However, please strive to find some cross-functional teammates as well. We encourage team members to nominate peers outside of their immediate functional group, especially if it is someone you have had trouble communicating with in the past. The best feedback can come from those you don't necessarily see eye-to-eye with. Managers will receive an email to review and approve of their direct report's peer selection. If the manager has not approved by the deadline, People Ops will override and approve to keep the feedback cycle moving.

**Q: What will 360 feedback be based on?**

**A:** Reviews will include the following five questions:

1. What's one thing this person should stop doing?
1. What's one thing this person should start doing?
1. What's one thing this person should continue doing?
1. In which GitLab value is this person the strongest? What is the impact of that?
1. In which GitLab value is this person struggling? How do you think they can improve?

**Q: How long should the response to each question be?**

**A:** Comments should be to the point and include specific examples. Each feedback form should be unique, therefore People Ops cannot link an example of a great manager to direct report feedback form, as it could be used as a template to copy and paste. Instead we encourage managers to reach out to People Ops to discuss any questions or work through the performance cycle.

**Managers:**
In cases where you’ve identified your top performer, we should learn from what makes that person successful to share with others. In cases where unsatisfactory performance is identified, you should plan to deliver a PIP to clearly identify performance gaps and expected changes.

### Using Lattice

**All Team Members**

Once People Ops launches a review cycle you should receive an email invitation from Lattice to begin the review process. From the email notification:
* Click ***Select Your Peers*** or ***Perform Reviews*** (at the bottom of the email).
* Login to Lattice.
* If not immediately directed to the Reviews tab:
  * Click ***Reviews*** (at the top of the Home screen) ***=> Review Cycles =>*** ***Active***
  * Click ***Select Your Peers*** or ***Perform Reviews*** begin self, manager, and peer reviews.
* All questions require an answer.
* Answers will auto save as you complete the review form.
* Click ***Save and Exit*** (at the top right corner) to save your work and come back later to finish your review.
* Click ***Submit Review*** when finished.
* Reviews are editable until the review cycle is closed.
* People Ops will publish a notice before closing a review cycle.
* Managers can view self reviews as they are submitted by their direct reports.

Once People Ops closes a review cycle:
* Managers will see a review packet for their direct reports.
* You can also download a PDF version of your direct report’s review packet.
* Direct reports will receive an email from Lattice when a review packet has been sent to you.
* Once you have clicked ***view direct reports*** then ***view feedback*** you will be able to add your manager remarks.
* Click ***Share with***  ____ to share the review packet with your direct reports (the review will not be shared until you take action).
* Once the review packet has been shared by the manager an email will be sent to their direct reports with a link for them to now view the completed the feedback form.

Late Reviewers (alternate manager):
* People Ops can add or update reviewers (past or present managers) during an active review cycle.
* If you are added as a ***Late Reviewer:***
* You will receive an email from Lattice asking that you provide feedback on a particular individual.
* To begin your review, click the link provided in the email.

Chat Feature:
* You have access to Lattice’s Customer Success team by clicking the ***Chat*** button in the bottom right hand corner of the screen (looks like a document with a smile)

Lattice Help Center Articles:
* [The (360) Review Process](https://help.lattice.com/admin-experience-admin-panel/reviews/the-360-review-process)
* [How to Perform a Review](https://help.lattice.com/employee-experience-you-page/reviews/how-to-perform-a-review)

If you have any questions or concerns as you go through the feedback cycle, please reach out to People Ops.

**People Ops**
* People Ops will update the reporting structure in Lattice before the review cycle is created by submitting a file feed through the admin portal.   
* People Ops will then create the cycle:
  1. Select Admin at the top of the screen.
  1. Click Create New Cycle
  1. Name the cycle, click Save and Continue.
  1. Select which employees will be part of the review.
  1. Choose the proper template for the review cycle.
  1. Check the box "Reviewees will select their own peers and get them approved by their manager."
  1. Under Visibility, select "Share all feedback with reviewers’ names attached" and "When writing, allow managers to see reviews written about their direct reports as they’re submitted."
  1. Disable Final Scores.
  1. Select Manual Sequence.
  1. Create Review Cycle and send out an informational email through Lattice.
* Once the peer selection deadline has passed, admins can override manager approval of peer selections to keep the review cycle moving.
* Follow-up with anyone who has yet to complete their reviews before the deadline for the cycle passes.

### 360 Feedback Meeting

This section outlines the conversation for a manager to have with their direct report after the cycle has closed. Peer feedback is not meant to evaluate performance, but instead assist in giving feedback to help the team member grow and develop within their role. 

* No surprises. Team members should never hear about positive or performance in need of improvement for the first time at the feedback meeting. Team members should have regular [1:1s](https://about.gitlab.com/handbook/leadership/1-1/) where this is discussed.
* The overall aim is to providing meaningful feedback. Don't allow the feedback meeting (document and conversation) to (d)evolve into a "todo" list.
* Share the form with the team member in advance of the meeting so they can prepare and come to the meeting with questions and discussion points.
* Make sure you (Manager) are also prepared for the discussion, write down some notes and key points you want to make.
* Make time to talk about the future - [career development](https://about.gitlab.com/handbook/leadership/1-1/#career-development-discussion-at-the-1-1) by working through the [Experience Factor Guidelines](https://about.gitlab.com/handbook/people-operations/global-compensation/#experience-factor-guidelines) to progress against those factors.
* Make sure to discuss and document on the team page any [expertises](https://about.gitlab.com/roles/expert/) the team member has obtained.
* If there are areas that need improvement, plan to deliver a [PIP](https://about.gitlab.com/handbook/underperformance/).
* This should be a conversation, try to avoid doing all the talking and get feedback from the team member. Ask questions such as:
   1. How can I support you with progressing to the next experience level for your position?
   1. How can I be a better manager for you?
   1. What are you hoping to achieve at GitLab this coming year?
* Avoid the [horns and halo effects of recent events](https://www.thebalance.com/effective-performance-review-tips-1918842), and instead make sure to take a step back to review the entire review period.
* Make sure you discuss _positive_ aspects of performance, but avoid using the ["feedback sandwich"](https://www.officevibe.com/blog/employee-feedback-examples) to mask an honest conversation about areas that need improvement
* Follow Up. Did you discuss pathways to career progress, or specific points of attention for improving performance? Make sure you add them to the top of the 1:1 doc so as to remind yourselves to follow up every so often.
