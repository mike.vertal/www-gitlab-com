---
layout: markdown_page
title: Learning and Development
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

Welcome to the Learning & Development (L&D) page at GitLab! L&D is an essential part of any organization's growth, success and overall business strategy. We want to support the development of our GitLabbers' competencies, skills and knowledge by providing them with the tools they need and also the opportunities to progress their own personal and professional development.  


## Career Mapping and Development
{:#career-mapping-and-development}

We have started this process at GitLab by defining Junior, Senior and Staff advancement levels. Career Mapping helps GitLabbers to understand and develop the skills they need to achieve their goals, giving them clear criteria.  
Mapping helps managers and leaders internally develop the skills and knowledge they need to achieve future business goals. The key to this is to identify the key skills, knowledge, and abilities needed to master each level. Another essential tool is a career development plan, here are some examples:

 - [UX Team Example](https://docs.google.com/spreadsheets/d/1GugUY_vPMERSP7QvQ7kaUrsaFn84YFipQtPZ1f5i_Q4/edit#gid=1712199154)
- [Career Plan](https://docs.google.com/document/d/1hJIzMnVhEz3X4k24oAwNnlgGhBeQ518Cps9kLVRRoWQ/edit)
- [Template  - Development Scorecard](https://docs.google.com/spreadsheets/d/1DBrukzzsV6InaCkZf8_ngLeTcLQ9uj6ynE93qLmHkQA/edit#gid=1677297587)
- [career plan template](https://performancemanager.successfactors.com/doc/po/develop_employee/carsample.html)

Managers should discuss career development at least once a month at the [1:1](https://about.gitlab.com/handbook/leadership/1-1/) and then support their team members with creating a plan to help them achieve their career goals. If you would to know more about this please checkout the [career mapping course video](https://www.youtube.com/watch?v=YoZH5Hhygc4)


## Language Courses

If you have any language courses you would like to recommend or links to websites please add them to this section.

 - [The 8 Best Interactive Websites for Adults to Learn English](https://www.fluentu.com/blog/english/best-websites-to-learn-english/)


## Common Ground: Harassment Prevention for Managers

All managers will be sent an invitation link from people ops to complete [this training](https://about.gitlab.com/courses/) using [Will Interactive's LMS](https://learning.willinteractive.com/). Once you receive the email please do the following:

1. Underneath the green **Sign In** box, click on the **Sign Up Now** link (also in green) which is right after *Don't have an account?*
1. Enter in your name and email address
1. Create a password
1. You may be sent a link to verify your account
1. Once you have logged in successfully you will be taken to your home screen
1. Once there you should see the course title **GitLab: Sexual Harassment Training**
1. Click on that to begin the training. This is 2 hrs long, but you can stop and come back to it.
1. You can also use the navigation bar at the top right-hand side of screen for volume and screen settings
1. To the left and right of the center screen you should see this symbol: > which you can click on to move forward and back through the training
1. People ops will send you a certificate of completion once the training has been completed. A copy of the certificate will be filed in BambooHR in the *verification* folder
1. To create the certificate, click on *view* in the course title
1. Scroll down to *users* then click on *completion certificates* to download the PDFs

If you have any questions or need further help please ping people ops in the `#manager_training` or `#peopleops` channels in slack.
