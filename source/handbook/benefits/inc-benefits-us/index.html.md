---
layout: markdown_page
title: "GitLab Inc (US) Benefits"
---

Can't find what you're looking for? Try the main [People Operations page](/handbook/people-operations).

## On this page
{:.no_toc}

- TOC
{:toc}

----

## Specific to US based employees
{: #us-specific-benefits}

US based employees' payroll and benefits are arranged through TriNet. The benefits decision discussions are held by People Operations, the CFO, and the CEO in the summer, to elect the next year's benefits by TriNet's deadline. People Operations will notify the team of open enrollment as soon as details become available.    

The most up to date and correct information is always available to employees through the [TriNet HRPassport portal](https://www.hrpassport.com) and the various contact forms and numbers listed there. This brief overview is not intended to replace the documentation in TriNet, but rather to give our GitLabbers and applicants a quick reference guide.

If you have any questions regarding benefits or TriNet's platform, feel free to call their customer solutions line at 800-638-0461. If you run into any issues with the platform itself, try switching from Chrome to Safari to login and enroll in benefits/view your profile. If you have any questions in regards to your TriNet paycheck log in to TriNet, then go to [How To Read Your Paycheck](https://www.hrpassport.com/Help/Docs/pdf/Readpaycheck_US.pdf).

Carrier ID cards are normally received within 2-3 weeks from when you submit your benefit elections. If you or your medical providers are in need of immediate confirmation of your coverage, please contact the carrier directly.

## Group Medical Coverage

_If you already have current group medical coverage, you may choose to waive or
opt out of TriNet's group health benefits. If you choose to waive health coverage,
you will receive a $300.00 monthly benefit allowance and will still be able to
enroll in optional plans and flexible spending accounts._

### Medical

TriNet partners with leading carriers, like Aetna, Florida Blue, Blue Shield of
California and Kaiser, to offer a broad range of medical plans, including high
deductible health plans, PPOs, and HMOs. Medical plan options vary by state, and
may also include regional carriers.

#### Obtaining Plan Cost Amounts

1. Go to HR Passport homepage.
1. Select Employee view.
1. Go to Benefits - Resources in the dashboard.
1. Select Plan Costs.
1. Build or maintain custom benefits summary.
1. Select the appropriate location.
1. A report will be generated with all plan costs available in the area.

#### Transgender Medical Services

Recently, the [United States Department of Health and Human Services](https://www.hhs.gov/) released a [mandate on transgender non-discrimination](http://www.transgendermandate.org/). As part of this mandate, medical carriers were given time to review their current policies and update to reflect the mandate. Expanded language for coverage should be visible in the 2017-10-01 renewal for TriNet’s plans.

Because there is a wide range of services, treatment, and goals for transgender patients, employees are encouraged to contact their carrier directly to have these discussions.

### Pregnancy & Maternity Care

With medical plans, GitLab offers pregnancy and maternity care. Depending on the plan you selected through TriNet, your coverages may differ for in-network vs out-of-network, visits, and inpatient care. Please contact TriNet at +1 800 638 0461 with any questions about your plan.

### Dental

TriNet's four dental carriers -Delta Dental, Aetna, Guardian Dental, and MetLife-
offer a high and a low national dental PPO plan. Aetna and Delta Dental also
will make available a DMO plan in many states.

### Vision

TriNet also offers a high and a low vision plan nationally through two different
carriers: Aetna and Vision Service Plan (VSP). These plan options ensure that
you can choose the best plan and carrier for your individual vision needs.

### Premiums

The Company pays 100% of premiums for medical, 100% of premiums for dental and
100% of premiums for vision coverage for employee. In addition, the Company
funds 50% of premiums for medical, 50% of premiums for dental and 50% of premiums
for vision coverage for spouse, dependent, and domestic partner. These contribution
amounts (monthly) are capped at:

| Insurance (2017-10-01 - 2018-09-30)| Company Contribution Cap |
| ---------------------------------- | -----------------------: |
| Medical Employee Only              |                  $547.00 |
| Medical Employee + Spouse          |                  $905.50 |
| Medical Employee + Child(ren)      |                  $831.50 |
| Medical Employee + Family          |                $1,102.50 |
| Group Dental Employee Only         |                   $24.67 |
| Group Dental Employee + Spouse     |                   $37.63 |
| Group Dental Employee + Child(ren) |                   $38.86 |
| Group Dental Employee + Family     |                   $51.81 |
| Group Vision Employee Only         |                    $9.17 |
| Group Vision Employee + Spouse     |                   $13.78 |
| Group Vision Employee + Child(ren) |                   $14.42 |
| Group Vision Employee + Family     |                   $20.30 |


You are responsible for the remainder of the premium cost, if any. If you do not enroll in a plan within your benefits election period, you will automatically receive the [medical waiver allowance](/handbook/benefits/#group-medical).

## Basic Life Insurance and AD&D

TriNet offers company paid basic life and accidental death and dismemberment (AD&D)
plans. The Company pays for basic life insurance coverage valued at $20,000, which
includes an equal amount of AD&D coverage.

## Short and Long-Term Disability Insurance

Disability insurance plans are designed to provide income protection while you recover
from a disability. This coverage not only ensures that you are able to receive some
income while out on disability; it also provides absence management support that helps
facilitate your return to work. TriNet offers several short and long term disability
plan options. Note: There are five states that have state-mandated disability insurance requirements
California, Hawaii, New Jersey, New York and Rhode Island. If you do not live in one of the listed
states it is recommended that you elect Short Term Disability Insurance through TriNet to protect
your financial situation. For more information on State Disability Insurance please contact People Ops.

### Group Long-Term and Short-Term Disability Insurance

The Company provides a policy that may replace up to 66.7% of your salary, up to
a maximum benefit of $12,500 per month, for qualifying disabilities. A waiting
period of 180 days will apply.

## 401k Plan

The company offers a 401k plan in which you may make voluntary pre-tax contributions
toward your retirement. We do not currently offer matching contributions. See the
[People Operations](https://about.gitlab.com/handbook/people-operations/) page for
details on eligibility and sign up.

### Administrative Details of 401k Plan

1. You are eligible to participate in GitLab’s 401k as of the 1st of the month after your hire date.
1. You will receive a notification on your homepage in TriNet Passport once eligible,
if you follow the prompts it will take you to the Transamerica website https://www.ta-retirement.com/
or skip logging in to TriNet Passport and go directly to https://www.ta-retirement.com/
after the 1st of the month after your hire date.
1. Once on the home page of https://www.ta-retirement.com/ go to "First Time User Register Here".
1. You will be prompted for the following information
   1. Full Name
   1. Social Security Number
   1. Date of Birth
   1. Zip Code
1. Once inside the portal you may elect your annual/pay-period contributions, and Investments.

## Employee Assistance Program

The Employee Assistance Program, or [EAP](https://drive.google.com/file/d/0Bwy71gCp1WgtRFg2a1BtNGRsUEU/view?usp=sharing), provides resources to help resolve personal concerns that may be affecting your health, well-being, family life, or job performance.

## Optional TriNet Plans Available at Employee Expense

### Flexible Spending Account (FSA) Plans

FSAs help you pay for eligible out-of-pocket health care and dependent day care expenses
on a pretax basis. You determine your projected expenses for the Plan Year and then
elect to set aside a portion of each paycheck into your FSA.

### Supplemental Life Insurance

If you want extra protection for yourself and your eligible dependents, you have
the option to elect supplemental life insurance. You may request coverage yourself
for one to six times your annual salary, with a maximum benefit of $2,000,000.
Spousal coverage is also available in increments of $10,000 up to $150,000, and
child coverage for $10,000. Note that amounts above guaranteed issue
($300,000 for you and $30,000 for your spouse) and certain coverage increases
must be approved by the insurance carrier.

### Supplemental Accidental Death and Dismemberment Insurance

AD&D covers death or dismemberment from an accident only. You may elect supplemental
AD&D coverage in amounts of $25,000, $50,000, $100,000, $250,000, $500,000 or $750,000.

## GitLab Inc. United States Leave Policy:

Based on the Family and Medical Leave Act, or [FMLA](https://www.dol.gov/whd/fmla/), US Employees are "eligible to take job-protected leave for specified family and medical reasons with continuation of group health insurance coverage under the same terms and conditions as if the employee had not taken leave." For more information on what defines an eligible employee and medical reason please visit the [Electronic Code of Federal Regulation](http://www.ecfr.gov/cgi-bin/text-idx?c=ecfr&sid=d178a2522c85f1f401ed3f3740984fed&rgn=div5&view=text&node=29:3.1.1.3.54&idno=29#sp29.3.825.b) for the most up to date data.

### Apply For Parental Leave in the US

1. The employee will fill out the [Extended Leave Request Form](https://drive.google.com/a/gitlab.com/file/d/0B4eFM43gu7VPdzhITFI2VDR2TVAxS2N6ZUlkVzZveTBWSkVR/view?usp=sharing) at least thirty days before the start of the leave and email the form to People Ops.
  * The reason for leave is "Maternity/pregnancy-related medical condition and/or to bond with a new child."  
  * If you would like your payroll deductions to continue to be deducted on each paycheck, select "I have made other arrangements with my Company."
1. Once the Extended Leave Request form has been submitted and approved by People Ops, it will then be entered into TriNet by People Ops.
  * Go to Passport under Find > Find Person by Name > Pull the employee up > Extended Leave Request.
  * Enter all applicable information.
  * If the employee is eligible for 100% of pay, make sure to note in the request “leave with pay” status for the employee.
  * Once the leave is in TriNet it takes the LOA department about 5 business days to process the request.
1. Once the Leave of Absence request has been approved through TriNet, GitLab will be notified via email with the Initial Notification letter which confirms the Extended Leave of Absence, the FMLA Notice, Designation, and Exhaust Notice Letters. These should be filed in BambooHR in the Benefits folder.
1. People Operations will forward the letters from TriNet via email to the employee.
  * If this is for a maternity leave, People Operations will explain the process for the employee to file for Short Term Disability (STD). [Short-Term Disability](/handbook/benefits/#short-and-long-term-disability-insurance) is processed through Aetna. If the employee is eligible for state STD, Aetna will advise on how to file. Provide the employee with Aetna's phone number and plan number, which can be found in the People Ops 1Password vault.
1. Once the employee has filed a claim for STD, they will need to confirm the start and end dates of the STD.
  * The typical pay for STD is six weeks for a vaginal birth and eight weeks for a c-section. STD can be extended if the employee has a pregnancy related complication (bed rest, complications from delivery, etc).
1. People Operations will confirm [payroll details](#payroll-processing-during-parental-leave) with the Controller.   
1. The employee will notify People Operations on their first day back to work.
1. People Operations will submit the return to work request in TriNet.
  * In the Beta site click on your Admin View at the top right.
  * Then in the menu to the left choose Employees > Manage Employees.  
  * Search and click on the employee returning from leave.  
  * Then click the Return from Leave button at the top right.  
  * Input all details and submit.  
  * Verify if a special payroll needs to be created for any days that were not paid if a payroll has already been processed. (Only applicable if the employee was not eligible for 100% of the wages throughout leave).

### Payroll Processing During Parental Leave

**Paternity Leave**
Paternity Leave is not covered under Short Term Disability, so if the employee is eligible for 100% of pay, payroll would enter 100% of the pay period hours under "Leave with Pay."   

**Maternity Leave**
For maternity leave, GitLab will verify 100% of the wages are paid for eligible employees through TriNet's payroll and Short-term Disability (STD).

1. While the employee is on short-term disability (which covers 66.7%), GitLab will supplement the remaining 33.3% through TriNet's payroll.
  * The employee will inform People Operations when their short term disability is set to begin and end.
  * There is a seven day elimination period for short term disability that is not paid through Aetna, where GitLab will need to supplement the entire wage through payroll.
    * For instance, if Aetna has approved STD for January 1 - February 12, January 1 - January 7 would not be paid through STD.
    * If any adjustments need to be made to a payroll that has already passed, People Ops will send an email with the employee's name and number of hours to be paid to the TriNet payroll contact (found in the People Ops 1Password vault).    
  * For the days the employee is paid through STD, payroll will adjust leave with pay hours to equal 33.3% and leave without pay hours to equal 66.7%.
    * For example, if there are 80 hours in the pay period you would input 26.66 hours Leave with Pay / 53.34 Leave without Pay.
1. When short-term disability ends, payroll will need to have 100% of the hours fall under leave with pay.
