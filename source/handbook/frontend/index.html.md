---
layout: markdown_page
title: "Frontend Group"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Teams

There are two teams within the Frontend group: General and Performance.

- General team is primarily focused on delivering product features (Discussion, CI/CD, Platform, Monitoring, Marketing)
- Performance team is primarily focused on improving frontend performance

There is a frontend group call every Tuesday, before the team call. You should have been invited when you joined; if not, ask your team lead!

[Find the product manager mapping to engineering teams in the product handbook](/handbook/product)

### Frontend Marketing
{: #marketing}

The Frontend Marketing team is responsible for all frontend content on the [about.gitlab.com](about.gitlab.com) website. This website is our homepage. It presents tons of different marketing material to many different users. On this site you will see:

* Our team page.
* Our blog.
* Ways to sign up for GitLab.
* Different talks GitLabbers are giving.
* Product comparisons.
* etc.

Marketing Frontend Engineers will be responsible for the overall looks of this site with the help of UX.

### Frontend team calls

The frontend team has scheduled weekly calls. During this call, team members are encouraged to share
information that maybe relevant to share with other members synchronously (Eg. new documentation change, new breaking changes added to `master`).

Starting in January 2018, we will also be adding a monthly theme to the first call of each month. The purpose of these themes is to add some
fun and quirkiness to our team calls (so that we can learn more about each other) but it shouldn't distract or derail into too much off-topic conversation.

**How does it work**
- Winner of the previous theme will determine the theme of the next month
- Theme will be announced a few weeks before the next month
- Everyone in the call should write down who they think is the winner on a piece of paper and reveal it all at once (to prevent voting bias) during the voting

| Date | Theme | Winner |
|---|---|---|
| 2018-01-09 | Most interesting hat | Jose Ivan Vargas |
| 2018-02-13 | Most interesting shirt/t-shirt (No GitLab swag) | Winnie |
| 2018-03-06 | Most interesting sunglasses | André |
| 2018-04-03 | Most peculiar trip souvenir | ? |

### Choosing something to work on

Prior starting your work on your `Deliverable` labeled issues take a look at the [`Next Patch Release ` issue list].

[`Next Patch Release ` issue list]: https://gitlab.com/groups/gitlab-org/issues?label_name[]=Next%20Patch%20Release&label_name[]=frontend

### Release planning

A few days after the 4th of each month, the Frontend team goes through all the issues that have been assigned to the next milestone by the Product Manager and that have the `Deliverable` label applied by the Backend Lead, regardless of any `frontend` label it may have.

If the issue requires some work from the Frontend team, and if there is enough capacity to accept the request, the `frontend` label is applied.
If, for any reason, an issue cannot be scheduled, a note should be added to inform everyone about that.
Obviously, when choosing which issue has to be rejected, the Frontend Lead can ask for more details to both the Product Manager or the Backend Lead that are responsible for the specific issue.
