---
layout: markdown_page
title: "Technical Account Management"
---

# Technical Account Management Handbook
{:.no_toc}

## On this page
{:.no_toc}

- TOC
{:toc}

### Role & Responsiblities
See the [Technical Account Manager job description](/roles/sales/technical-account-manager/)

### Binding Agent
Technical Account Managers at GitLab fill a unique space in the overall service lifecycle and customer journey.  They can be considered the "glue" actively binding together sales, solution architects, customer stakeholders, product management, implementation specialists and support.

### Trusted Advisor
GitLab's Technical Account Managers (TAM's) serve as trusted advisors to GitLab customers offering guidance, planning and oversight during the technical deployment and implementation process.  A TAM may have a specific methodology they hold dear and use during the onboarding process or they may adapt to the customer approach and in both cases the process is highly collaborative.

### Advocate
A Technical Account Manager is an advocate for both the customer and GitLab. They act on behalf of customers serving as a feedback channel to development and shaping of the product.  In good balance, they also advocate on behalf of GitLab to champion capabilities and features that will improve quality, increase efficiency and realize new value for our customer base.

### TAM Responsibilities
The Technical Account Manager is the key partner helping customers achieve their strategic objectives and maximum value from their investment in GitLab. Additionally, the TAM serves as the liaison between the customer and the GitLab ecosystem, streamlining collaboration with Product Management, Engineering, Sales, Professional Services and others.

* Contact new customer within x days of purchase
* Own, manage, and deliver consistent onboarding experience based on tier
* Achieve onboarding milestones
* Send satisfaction survey 90 days post sale
* Actively build and maintain trusted and influential relationships
* Deliver consistent engagement model by tier
* Serve as a main POC
* Increase integrations, identify additional champions, drive utilization
* Help customer realize the value of investment
* Actively seek expansion opportunities
* Identify all impediments

---
### Transition and Handoff Workflow

In an effort to capture the wider scope of customer interactions with GitLab, the flow outlined below will soon addresses the business processes involved through the entire service lifecycle and customer journey.  There is an active initiative to unify stages, phases, titles, names, etc led by Marketing and Sales.

### Responsibility Matrix and Transitions

The table below depicts the relationships between activities, responsible roles, relationship and opportunity stages in the customer lifecycle.  A more detailed in the [responsibility assignment matrix (RACI)](https://en.wikipedia.org/wiki/Responsibility_assignment_matrix) format will be an critical tool for every GitLab customer.

|  **Sequence** | Activity                                    | Who's Responsible | Business Relationship Stage | Opportunity Stage | Output  |
|  ------: | :------ | :------ | :------ | :------ | :------ |
|  **1** | Create relationship lifecycle meta-record (e.g., SFDC) | Account Executive | Lead or Contact | Discovery | A data record containing information related to the prospect and having data fields attributes that will support the long term ability to "current state" of a customer at any given point in the lifecycle.  This cell links to an example customer "meta-record" |
|  **2** | Qualify the lead in terms of GitLab value to their organization | Account Executive | Lead or Contact | Discovery |  |
|  **3** | Engage solution architects  | Account Executive | Lead or Contact | Discovery |  |
|  **4** | Intro and engage solution architect in technical conversations | Account Executive | Lead or Contact | Discovery |  |
|  **5** | Update relevant SFDC fields with new information | Account Executive | Opportunity | Scoping |  |
|  **6** | Demo GitLab tech and articulate the value proposition | Solution Architect | Opportunity | Scoping |  |
|  **7** | High-level technical discovery and fit assessment | Solution Architect | Opportunity | Technical Evaluation | Updates to the customer technical profile that demonstrate the architectural fit is feasible and provides sufficient information for generating a SOW and solution design blueprint. |
|  **8** | Capture environment and technical specifics for each prospect | GitLab Group | Opportunity | Technical Evaluation | Enriched customer meta-record |
|  **9** | Create a solution design blueprint from requirements gathering, tech discovery and customer meta-record | Solution Architect | Opportunity | Technical Evaluation | Blueprint solution design diagram |
|  **10** | Handoff solution design blueprint to IE and TAM | Solution Architect | Opportunity | Technical Evaluation | IE and TAM full review |
|  **11** | Validate the solution design as generated by SA | Technical Account Manager | Opportunity | Proposal | Approval / signoff by IE and TAM |
|  **12** | Create and submit professional services implementation SOW to stakeholders | Implementation engineer | Opportunity | Proposal | Statement of Work (inclusive of blueprint solution design) |
|  **13** | Create and submit Proof of Value (PoV/PoC) plan to stakeholders  | Solution Architect | Opportunity | Technical Evaluation | PoV plan (inclusive of blueprint solution design) |
|  **14** | Introduce TAM to customer stakeholders | Account Executive | Opportunity | Technical Evaluation | Face to face is ideal and Zoom will work based on the circumstances |
|  **15** | Agreed upon success criteria | GitLab Group | Opportunity | Negotiating | A document that clearly articulates what done looks like and when done is successful.  The success criteria are required for proof of value or a professional services implementation.  The document should be signed by an accountable individual from both parties and verbally discussed. |
|  **16** | Close with a technical win, negotiated terms and signed contract | GitLab Group | Opportunity | Closed Won | Contract, MSA, SLA, OLA, et al the other legal bits |
|  **17** | [PoV/PoC project kick-off](https://about.gitlab.com/handbook/sales/POC/) | Technical Account Manager | Opportunity | Negotiating | Kick-off and proof of value execution plan catered to the customer |
|  **18** | Professional Services Implementation kick-off | Technical Account Manager | Customer | Closed Won | Kick-off and implementation plan catered to the customer |
|  **19** | Transition customer meta-record and technical profile to IS or TAM | Solution Architect | Customer | Closed Won |  |
|  **20** | Transition customer meta-record and business profile to TAM | Account Executive | Customer | Closed Won |  |
|  **21** | Manage the ongoing customer meta-record rigorously | Technical Account Manager | Customer | Closed Won |  |

# Engagement Models

There are three models currently offered for TAM engagement.  These are broken into tiers that currently use Annual Recurring Revenue as a metric for determining a manageable volume for a single TAM and the depth of involvement during the engagement.

## Table of TAM Engagements

||TIER 1 |TIER 2 |TIER 3 |
|:---|:---|:---|:---|
|Minimum ARR|$500,000|$250,000|$100,000|
|Accounts per TAM|5|10|25|
|Customer Intro|Phone introduction to TAM prior to final sale|Email introduction to TAM prior to final sale|Email introduction to customer success department|
|Onboarding|Personalized, individual onboarding with milestones and goals by way of success plan|One-to-many onboarding program via email campaign with individual milestones and goals|One-to-many onboarding program via email campaign with individual milestones and goals|
|New Users|Personalized onboarding for new users above x seats|One-to-many onboarding program via email campaign|One-to-many onboarding program via email campaign|
|Success Plan Review|Quarterly|
|Roadmap|Quarterly|Quarterly|new feature video and/or links to youtube videos|
|Content|Email content shared regularly to drive 1-on-1 engagements|Email conent shared regularly to drive one-to-many engagement such as webinars, community evens|Automated email content shared regularly such as newsletters|

## Managing the Customer Engagement

Technical Account Managers will manage the customer engagements using a GitLab project in the [`account management` group](https://gitlab.com/gitlab-com/account-management/).  This project will be based off a [customer collaboration project template](https://gitlab.com/gitlab-com/account-management/customer-collaboration-project-template) and then customized to match the customer's needs as outlined above. The project is pre-loaded with milestones, issues, labels and a README template to help kick off the project and outline a proof of concept, implementation and customer onboarding.

### To start a new customer engagement:

1.  Somewhere between step 3 and step 7 of the customer journey sequence, a Solutions Architect should create a project for the customer in GitLab and include an Implementation Engineer and Technical Account Manager who're best aligned with the customer account.
2. Go to the [customer collaboration project template project](https://gitlab.com/gitlab-com/account-management/customer-collaboration-project-template).
3. Follow the steps in the initial README.md file to create the appropriate project type (post sales or pre sales).

### Where does a Technical Account Manager fit in?

During the pre-sales process, a Solutions Architect owns the project with assistance from the Strategic Account Leader, the Implementation Engineer if there is one assigned. Until the account becomes a paying customer, the project remains in pre-sales. Once the customer has paid, the Strategic Account Leader will set up the Welcome to GitLab call along with the key GitLab employees (SAL, SA, IE and TAM) and the customer. There is a preloaded issue for this in the project template.

This call will introduce the customer to the Technical Account Manager and begin the handover process. The Technical Account Manager will then lead the rest of the call and own the project going forward. The project is then moved from a pre-sales project under the [`pre-sales account-management` group](https://gitlab.com/gitlab-com/account-management/pre-sales) to a post-sales project under [`account-management` group](https://gitlab.com/gitlab-com/account-management).

## The Customer Meta-Record

The concept of a customer meta-record is how a GitLab TAM develops and maintains a holistic understanding of customer accounts.  It is a living record that includes both business and technical information about the customer.  The data captured here informs not only the TAM, but all of GitLab about critical details to the success of our customers.

## Business Profile

The business profile is captured during the discovery and scoping stages in the pre-sales phase of the customner lifecycle.  Once an opportunity is closed-won, the customer success TAM is largely responsible for maintaining this data.  The business profile of a customer's meta-record contains data points related to their organizational structure, the internal advocate/champion, the features most compelling to the customer, the "why" of purchasing GitLab, objectives for the implementation and critically, the data captured in the customer feedback loop.

## Technical Profile

The technical profile includes objective data points about a customers technical landscape and can be captured at any stage of the customer lifecycle.  Specifically, items related to architecture, sizing, scale, security and compliance requirements are captured in the technical profile.  In general, solution architects and implementation specialists are primarily responsible for collecting and capturing information for the technical profile.  This information is transitioned to the TAM once a customer goes live and the TAM maintains the information throughout the remainder of the customer lifecycle.

## Automated Customer Discovery Tools
A series of data sheets and automation scripts is being developed that will streamline much of the intake, discovery and reconnaissance activities during the pre-sales stages of the customer lifecycle.  Decisions surrounding where this data will reside and which information is part of the critical path during customer onboarding are currently underway.
