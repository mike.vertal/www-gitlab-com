---
layout: markdown_page
title: "Leadership"
---

## On this page
{:.no_toc}

- TOC
{:toc}

This page contains leadership pointers.
The first couple of headers indicate what group it applies to.
These use the groupings as defined on our [team structure page](https://about.gitlab.com/team/structure/).

## Team members

1. At GitLab leadership is requested from everyone, whether an individual contributor or part of the leadership team.
1. As a leader, GitLabbers will follow your behavior, always do the right thing.
1. Everyone that joins GitLab should consider themselves to be an ambassador of our [values](https://about.gitlab.com/handbook/values) and a protector of our [culture](https://about.gitlab.com/culture/).
1. Behavior should be consistent inside and outside the company, don't fake it outside, just do the right thing inside the company as well.
1. GitLab respects your judgment of what is best for you, afterall you know yourself best. If you have a better opportunity somewhere else don't stay at GitLab out of a sense of loyalty to the company.
1. In tough times people will put in their best efforts when they do it for each other.
1. We work async, lead by example and make sure people understand that things need to be written down in issues as they happen.
1. We are not a democratic or consensus driven company. People are encouraged to give their comments and opinions. But in the end one person decides the matter after they have listened to all the feedback.
1. It is encouraged to disagree and have a constructive confrontation but please [argue intelligently](https://www.brainpickings.org/2014/03/28/daniel-dennett-rapoport-rules-criticism/).
1. We value truth seeking over cohesion.
1. We avoid meetings because those don't support the asynchronous work flow, are hard to conduct due to timezone differences and are limited only to those attending them, making it harder to share.
1. Start meetings on time, be on time yourself, don't ask if everyone is there, and don't punish people that have shown up on time by waiting for anyone or repeating things for people that come late. When a meeting unblocks a process or decision, don't
celebrate that but instead address the question: how can we avoid needing a meeting
in the future to unblock?
1. We give feedback, lots of it, don't hold back on suggestions to improve.
1. If you meet external people always ask what they think we should improve.
1. As [Paul Graham said](https://twitter.com/paulg/status/802102152319606784): strive to make the organization simpler.
1. Saying something to the effect of "as you might have heard", "unless you've been living in a cage you know", "as everyone knows", or "as you might know" is toxic. The people that know, don't need it to be said. The people that don't know feel like they missed something. They will be afraid to ask about the context since they don't want to look like they are out of the loop.

## Management team

1. When times are great be a voice of moderation, when times are bad be a voice of hope.
1. To maintain an effective organization, a managers span of control should be around 7, ranging anywhere from 4 to 10. Below this range the inefficiency of an extra organizatinal layer is larger than the benfit of a specialized group. Above this range the manager don't have time to do proper 1:1's anymore.
1. If you praise someone try to do it in front of an audience. If you give suggestions to improve, do it 1 on 1.
1. As soon as you know you'll have to let someone go, do it immediately. The team member is entitled to know where they stand. Delaying it for days or weeks causes problems with confidentiality (find out that they will be let go), causation (attributing it to another reason), and escalation (the working relationship is probably going downhill).
1. When performance or behavior is below par or not in line with our [values](https://about.gitlab.com/handbook/values) we normally put someone on a [performance improvement plan (PIP)](https://about.gitlab.com/handbook/underperformance/). However there are some exceptions to following the PIP process but in all cases managers should speak to the people operations generalist or Chief Culture Officer early on to evaluate the best solution.
1. Not all underperformance should be in a PIP. You may just need to realign them. Take off some tasks and see if they improve.
1. When addressing underperformance, don't wait. Set appropriate goals upfront, both parties are clear.
1. Understand that there are different ways to get to the same goal. There are different perspectives and discussions need to happen.
1. When someone says they are considering quitting drop everything and listen to them. Ask questions to find out what their concerns are. If you delay, the person will not feel valued and the decision will be irreversible.
1. In addition to announcing new team member arrivals, departures are also announced in the #general chat channel (once the Google Slack accounts are blocked, see the [offboarding page](handbook/offboarding) and the [offboarding checklist](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/offboarding.md for details). We must respect the privacy of the individual concerned. If you are asked why someone has left or is leaving, please refer that person to the [general guidelines](/handbook/general-guidelines/#not-public) section of the handbook where we describe what can and cannot be shared.
1. People should not be given a raise or a title because they ask for it or threaten to quit. We should pro-actively give raises and promote people without people asking. If you do it when people ask you are disadvantaging people that don't ask and you'll end up with many more people asking.
1. Don't refer to GitLabbers [as family](https://hbr.org/2014/06/your-company-is-not-a-family). It is great that our team feels like a close-knit group and we should encourage that, this builds a stronger team. But _families_ and _teams_ are different. _Families_ come together for the relationship and do what is critical to retain it. _Teams_ are assembled for the task and do what is required to complete it. Don't put the relationship above the task. Besides, families don't have an an [offboarding process](https://about.gitlab.com/handbook/offboarding/), families should have unconditional love, teams have conditional love. [The best companies are supporters of families.](https://twitter.com/myriadwill/status/917772249624702976)
1. Praise and credit the work of your reports to the rest of the company, never present it as your own. This and many other great lessons in [an ask metafilter thread worth reading](http://ask.metafilter.com/300002/My-best-manager-did-this).
1. Try to be aware of your [cognitive biases](https://betterhumans.coach.me/cognitive-bias-cheat-sheet-55a472476b18).
1. Great article about [how to think about PIPs](https://mfbt.ca/how-i-talk-to-leaders-about-firing-people-8149dfcb035b), although our time scales are shorter.
1. Do everything to unblock people. For example, if someone has a question that is keeping them from being productive, try to answer the question or find someone who can.
1. Communicate in a professional manner as though your words will be shared widely (e.g. published in a newspaper).
1. Employ multimodal communication to broadcast important decisions. To reach our distributed organization announce important decisions on the team call, email the appropriate team email lists, Slack the appropriate channels, and target 1:1's or other important meetings on the same day, with the same information
1. You are expected to [respond on social media](https://about.gitlab.com/handbook/marketing/social-media-guidelines/).
1. Make sure your reports experience a [sense of progress](http://tomtunguz.com/progress-principle/) in their work.
1. A tweet by [Sam Altman](https://twitter.com/sama/status/804138512878473220) combined the reply by [Paul Graham](https://twitter.com/paulg/status/804209365305659392) says it best: "People either get shit done or they don't. And it's easy to be tricked because they can be smart but never actually do anything." Watch for results instead of articulate answers to questions, otherwise you'll take too much time to identify under-performers.
1. [Our summits](https://about.gitlab.com/culture/summits) are meant for informal communication and bonding across the company. There is a limited time for business activities during the summit so all our regular meetings should happen outside of the summit. We want informal, cross team, open-ended meetings, that includes individual contributors. For example, inviting everyone including sales to suggest currently missing functionality in GitLab. Formal, team restricted, structured meetings, where not everyone is welcome shouldn't happen. For example, an executive team meeting to set the yearly budget. Never delay a decision until the summit, if anything use the summit as a deadline to get things done earlier.
1. We don't have explicit 20% time at GitLab. We measure results and not hours. If people are getting good results in the work that is assigned to them they are free to contribute to other parts of the company or work on a pet project. Don't say "your work on the pet project is hurting your performance" but say "we agreed to getting X done but it is delayed, what happened and how can I help?".
1. Pick a metric before launching something new. 9 out of 10 launches fail. If a project is not working out shut it down completely. Starving a team of headcount to have it die a slow death is not frugal nor motivating. Fund the winners which will still take years to break even.
1. Do not discuss raises in advance because the salary calculator may change before the amount of the raise is decided.
1. Instead of prescribing a direction to your reports it is best to ask ask questions following the [Socratic debate](https://en.wikipedia.org/wiki/Socratic_method) until you're happy with the direction. Your reports will have deeper knowledge in a more narrow area, so it is easy to draw a different conclusion because they base it on different data. That is why the questions are so important.
1. From the [common injunction at Berkshire](https://www.hb.org/the-psychology-of-human-misjudgment-by-charles-t-munger/): "Always tell us the bad news promptly. It is only the good news that can wait.". Make sure to inform your manager of bad news as quickly as possible. Promptly reporting bad news is essential to preserving the trust that is needed to recover from it.
1. Try to avoid military analogies. We're not an army, we're not at war, there is no battle, we're not killing anyone, and we don't have weapons. Military language is [not inclusive](https://www.london.edu/faculty-and-research/lbsr/killing-the-competition) and can lead to zero sum thinking. We take competing and winning very seriously, but there is no need to describe physical violence. Similarly, non-collaborative and aggressive terms like "rockstar" and "badass" put up walls between people. If a term is are standard in the industry, for example [killing a Unix process](https://shapeshed.com/unix-kill/#how-to-kill-a-process), it is acceptable to use it because that is more efficient. Do use primary-secondary instead of master-slave for replication mechanisms.

## Director team

1. Ability to align the day-to-day execution to the top objectives of the company.
2. Work peer-to-peer and sponsor healthy conflict amongst the team to resolve issues quickly, escalating only when all options are exhausted.

## Executive team

1. Like everyone at the company they live our values https://about.gitlab.com/handbook/values/
1. Like all leaders they follow our leadership principles https://about.gitlab.com/handbook/leadership/
1. They suggest relevant, ambitious, and quantifiable OKRs, and achieve 70% of them.
1. They are reliable and ensure their teams completes what they agreed to do.
1. They detect and communicate problems in their departments before other departments even notice them.
1. They hire and retain people that perform better at their jobs than they do.
1. They get a huge amount of things done by iterating fast and train their department in iteration.
1. VPs define and communicate on the business strategy and vision instead of completely being tactically in the business.
1. VPs share insights about other functional areas that make other VPs better at their job
1. VPs suggest and implement improvements to our cross functional processes.

## C-team

1. Have broad responsibilities, maximizing the scope of their discipline.
1. Frequently achieve results outside their discipline.
1. Make other executives better in their discipline.

## Making Decisions

1. We combine the best of hierarchical and consensus organizations. Hierarchical organizations have good speed but are bad at gathering data, leading to people saying yes but not doing it. Consensus organizations are good at gathering data but lack speed, leading to projects happening under the radar. We split decisions in two phases. The data gathering phase has the best of consensus organizations, everyone can contribute. The decision phase has the best of a hierarchical organization, the person that does the work or their manager decides what to do.
1. At GitLab, decision making is based on an informed and knowledgeable hierarchy. Not on consensus or democracy. Voting on material decisions shows a lack of informed leadership.
1. Make data driven decisions but consider environments that do not allow reliable data collection. According to research in this [article by the Harvard Business Review](https://hbr.org/2016/02/the-rise-of-data-driven-decision-making-is-real-but-uneven), "experience and knowledge of leaders in the subject matter still outperforms purely data-driven approaches."
1. Be aware of your unconscious biases and emotional triggers.
1. We don't have project managers. Individual contributors need to manage themselves. Not everyone will be able to do this effectively and be fit for our organization. Making someone responsible for managing others will make the job of the people that can manage themselves worse. If you manage yourself you have a much greater freedom to make decisions, and those decisions are based on deep knowledge of the situation. We want to retain the people that can handle that responsibility and therefore we can't retain the ones that struggle. Assigning a project manager/coordinator/case manager/etc. to something is an indicator that something is wrong and we are picking the wrong solution.
1. The person that does the work makes the decisions. While the decision maker only needs direct approval from their manager, they should listen to the data and informed opinions of others to arrive at their decision. Part of making good decisions is knowing who has good information and/or experience that informs that decision.

## Giving Performance Feedback

Giving regular feedback is extremely important for both managers and team members. Feedback can take the form of coaching sessions separate from the [1-1](/handbook/leadership/1-1). Giving feedback is also about being prepared and depending on the situation you should create separate invite with an agenda and structure it as follows:

- Provide context
- Put your feedback into three areas: Start, Stop, Continue
- Ask yourself, is this:
  - Actionable
  - Specific
  - Objective
  - Fair
  - Relevant to the job role and [experience factors](https://about.gitlab.com/handbook/people-operations/global-compensation/#experience-factor-guidelines)

  Ask questions listed in the [1-1](/handbook/leadership/1-1)) and the [career development discussion at the 1-1](/handbook/leadership/career-development-discussion-at-the-1-1) section.

### Identifying Root Cause

Sometimes when performance dips the best way to tackle it is to try to determine the root cause. This is easier said that done. There is a great tool that the [CEB (now Gartner)](https://www.cebglobal.com/) have come up with which may help assist with this. It is called [performance issue root cause diagnostic](https://drive.google.com/file/d/1wKWu7v-H1qgvkv2jtjGMBVBbEX_CeZ8X/view?usp=sharing). It may not always be possible/appropriate to determine the root cause so the [underperformance process](https://about.gitlab.com/handbook/underperformance/#introduction) should be followed.

## 1-1

Please see [/handbook/leadership/1-1](/handbook/leadership/1-1).

## No matrix organization

1. We believe everyone deserves to report to exactly one person that knows and understands what you do day to day. [The benefit of having a technically competent manager is easily the largest positive influence on a typical worker’s level of job satisfaction.](https://hbr.org/2016/12/if-your-boss-could-do-your-job-youre-more-likely-to-be-happy-at-work) We have a simple functional hierarchy, everyone has one manager that is experienced in their subject matter. Matrix organizations are too hard to get right.
1. We don't want a matrix organization where you work with a lead day to day but formally report to someone else.
1. The advantage of a functional structure is that you get better feedback and training since your manager understands your work better than a general manager.
1. For the organization, forgoing a separate class of managers ensures a simple structure with clear responsibilities.
1. A functional organization structure mimics the top structure of our and many other organizations (Finance, Sales, Engineering, etc.).
1. It reduces compensation costs, coordination costs, and office politics.
1. The disadvantage is that your manager has a limited amount of time for you and probably has less experience managing people.
1. To mitigate these disadvantages we should offer ample training, coaching, support structures, and processes to ensure our managers can handle these tasks correctly and in limited amount of time.
1. Everyone deserves a great manager that helps you with your career, lets you know when you should improve, hires a great team, and motivates and coaches you to get the best out of you.
1. "Nuke all matrices. Nuke all dual reporting structures. And nuke as many shared services functions as you possibly can." In the great [guide to big companies from Marc Andreessen](http://pmarchive.com/guide_to_big_companies_part2.html) (the other guides are awesome too).
1. On this page we recommend to read High Output Management and its author coined Grove's law: All large organizations with a common business purpose end up in a hybrid organizational form. We believe a dual reporting structure is inevitable, we just want to delay it as long as possible.
1. We want to promote organic cross-functional collaboration by giving people stable natural counterparts. Each Strategic Account Leader (SAL)works with one Sales Development Representative (SDR). Every backend team of developers maps to a [Product Manager (PM)](https://about.gitlab.com/roles/product/product-manager/), a [frontend team](https://about.gitlab.com/handbook/frontend/#teams).
1. We do make features with a ad-hoc cross-functional group called a [crew](https://about.gitlab.com/team/structure/#crew). This group is not a permanent team reporting to a project manager. The group consists of people that are assigned by their functional manager to work on an issue at the beginning of our monthly sprint. In practice it make sense to assign the same people if possible. But the composition of the group is variable. This is more flexible in case people are not available, if there is more work for a certain functional group, or when a specific expertise is needed. An example of a crew is the people working on our project to migrate GitLab.com to Google Cloud Platform who are from the production, build, database, and Geo groups.
1. Having a crew requires a lot of discipline from the people in that group to work together. People need to be a manager of one. There is no safety net in the form of a project manager. It means that people that do not manage themselves well are not a good fit. It also means that people that do manage themselves well have more freedom.
1. Functional companies are easier when you focus on one product. Apple focusses on the iPhone and can have a [unitary/functional/integrated organizational form](https://stratechery.com/2016/apples-organizational-crossroads/). The advantage is that you can make one strong integrated product. But we can maintain a functional organization as long as we keep offering new functionality as features of GitLab instead of different products. The fact that we're in touch with the market by using our own product also helps.
1. Having functional managers means that they rarely manage 100% of their time. They always get their hands dirty. Apart from giving them relevant experience that also focuses them on the output function more than the process. Hopefully both the focus and not having a lot of time for process reduces the amount of politics.

## Factory vs. studio

We want the best combination of [a factory and a studio](https://medium.com/@mcgd/factory-vs-studio-fb83b3fe9e14). The studio element is that anyone can chime in about anything, from a user to the CEO. Please step outside your work area and contribute. The factory element is that everyone has a clearly assigned task and authority.

## Process got a bad rep

Process has a bad reputation. It has that reputation for things that we try to avoid doing at GitLab. When you have processes that are not needed it turns into a bureaucracy. A good example are the approval processes. We should keep approval processes to a minimum. Both by giving people the authority to decide by themselves and by having a quick lightweight approval process where needed.

But process also has good aspects. Having a written down process how to communicate within the company greatly reduces time spend on on-boarding, increases the speed, and prevents mistakes. A counterintuitive effect is that it also makes it easier to change processes. It is really hard to change a process that doesn't have a name or location and lives in different versions in the heads of people. Changing a written process and distributing the [diff](https://en.wikipedia.org/wiki/Diff_utility#Usage) is much easier.

## Articles

1. [eShares Manager’s FAQ](https://readthink.com/a-managers-faq-35858a229f84)
1. [eShares How to hire](https://blog.esharesinc.com/how-to-hire/)
1. [How Facebook Tries to Prevent Office Politics](https://hbr.org/2016/06/how-facebook-tries-to-prevent-office-politics)
1. [The Management Myth](http://www.theatlantic.com/magazine/archive/2006/06/the-management-myth/304883/)
1. [Later Stage Advice for Startups](http://themacro.com/articles/2016/07/later-stage-advice-for-startups/)
1. [Mental Models I Find Repeatedly Useful](https://medium.com/@yegg/mental-models-i-find-repeatedly-useful-936f1cc405d)
1. [This Is The Most Difficult Skill For CEOs To Learn](http://www.businessinsider.com/whats-the-most-difficult-ceo-skill-managing-your-own-psychology-2011-4)
 1. Great article about [how to think about PIPs](https://mfbt.ca/how-i-talk-to-leaders-about-firing-people-8149dfcb035b), although our time scales are shorter.
1. [Impraise Blog: 1-on-1s for Engaged Employees](https://blog.impraise.com/360-feedback/1-on-1s-for-engaged-employees-how-good-managers-should-do-them-performance-review)
1. [Mind Tools: Giving Feedback: Keeping Team Member Performance High, and Well Integrated](https://www.mindtools.com/pages/article/newTMM_98.htm)
1. [Remote.Co: 5 Tips for Providing Feedback to Remote Workers](https://remote.co/5-tips-for-providing-feedback-to-remote-workers/)
1. [Really interesting blog post from Hanno on remote team feedback](https://hanno.co/blog/remote-team-feedback/)
1. [51 questions to ask in one-on-ones with a manager](https://getlighthouse.com/blog/questions-ask-one-on-ones-manager/)
1. [HBR: The rise of data driven decision making is real but uneven](https://hbr.org/2016/02/the-rise-of-data-driven-decision-making-is-real-but-uneven)
1. [Forbes: 6 Tips for Making Better Decisions](https://www.forbes.com/sites/mikemyatt/2012/03/28/6-tips-for-making-better-decisions/#966eb3b34dca)

## Books

Note: Books in this section [can be expensed](https://about.gitlab.com/handbook/spending-company-money).

1. High Output Management - Andrew Grove ([top 10](https://getlighthouse.com/blog/andy-grove-quotes-leadership-high-output-management/))
1. The Hard Thing About Hard Things: Building a Business When There Are No Easy Answers - Ben Horowitz
1. [The score takes care of itself - Bill Walsh](http://coachjacksonspages.com/The%20Score%20Takes%20Care.pdf)
1. Crucial Conversations: Tools for Talking When Stakes Are High - Kerry Patterson
2. The Advantage: Why Organization Health Trumps Everything Else In Business - Patrick Lencioni

## Training

We've created several of our own Leadership courses, see the [courses](/courses) page for more details.
This content will be moved to this page.

When you give a leadership training please [screenshare the handbook instead of creating presentation](/handbook/handbook-usage/#screenshare-the-handbook-instead-of-creating-a-presentation).

## Mentoring
To leverage the available experience within our management team, managers of
different experience levels have been paired up to become mentors for each other.
The pairings are listed in the "Manager Team Assignment" Google spreadsheet.

## People Operations

Feel free to reach out to any of the People Operations Team for further support on leadership development topics.  You can find us on the [team page](https://about.gitlab.com/team/), search for `People Operations`.
