---
layout: markdown_page
title: "Business Operations"
---

## On this page
{:.no_toc}

- TOC
{:toc}  


## What is the difference between Sales Ops, Marketing Ops, and Business Ops?
<table>
  <tr>
    <th>Business Operations</th>
    <th>Sales Operations</th>
    <th>Marketing Operations</th>
  </tr>
  <tr>
    <td>Answering the question: “How do we build go-to-market infrastructure to support company growth and pivots?”</td>
    <td>Quota assignments</td>
    <td>Day-to-day program and marketer support, including list uploads, landing pages, Marketo program management</td>
  </tr>
  <tr>
    <td>Strategic analytics and data management</td>
    <td>Field &amp; RD Forecasting</td>
    <td>Lead management</td>
  </tr>
  <tr>
    <td>Cross-functional process automation and optimization</td>
    <td>Sales Compensation</td>
    <td>Data enrichment</td>
  </tr>
  <tr>
    <td>New business technology review</td>
    <td>Sales admin activities</td>
    <td>Sales/Marketing SLAs</td>
  </tr>
  <tr>
    <td>Holistic business systems administration, development, integration, architecture, and planning around the entire GTM tool stack.</td>
    <td>Ad hoc analysis for CRO</td>
    <td>Marketing process design</td>
  </tr>
  <tr>
    <td>Prioritization of business operations issues</td>
    <td>Sales Process Training/Documentation</td>
    <td>Marketing process training and documentation</td>
  </tr>
  <tr>
    <td>Salesforce administration, development, architecture, customizations, and  technical documentation</td>
    <td>Sales Enablement functions</td>
    <td>Prioritization of Marketing-related business operations issues</td>
  </tr>
  <tr>
    <td>Customer portal integrations</td>
    <td>Sales training on process flows</td>
    <td></td>
  </tr>
  <tr>
    <td>Integration processes with license app and signups</td>
    <td>Handbook documentation</td>
    <td></td>
  </tr>
  <tr>
    <td>Marketing tech stack implementations, development, integrations, and</td>
    <td>Reporting and business analysis related to the sales function</td>
    <td></td>
  </tr>
  <tr>
    <td>Reporting infrastructure</td>
    <td>Prioritization of Sales-related business operations issues.<br>This means consolidating all of the requests from sales into a single funnel with prioritization.</td>
    <td></td>
  </tr>
</table>


## Purpose   
Overview of systems, workflows and processes for the three functional groups - [Sales](/handbook/sales), [Marketing](/handbook/marketing) and [Customer Success](/handbook/customer-success/) - that work closely together. This section is a singular reference point for operational management of the database and where the most up-to-date workflows, routing, rules and definition sets are managed.  Tracking all tools, license allocation, admins and contracts for the combined Go-To-Market tech stack. Tips, tricks and how to's for the various applications and external resources that will help user be more productive in the business systems.    


## Reaching the Teams (internally)   
- **Public Issue Tracker**: A short list of the main, public issue trackers. For respective teams, there may be additional resources. Please use confidential issues for topics that should only be visible to team members at GitLab
    - [Sales](https://gitlab.com/gitlab-com/sales/issues/) - general sales related needs & issues
    - [Salesforce](https://gitlab.com/gitlab-com/salesforce/issues) - Salesforce specific needs, issues and questions
    - [Marketing](https://gitlab.com/gitlab-com/marketing/issues) - All issues related to website, product, design, events, webcasts, lead routing, social media and community relations
    - [Customer Success SA Service Desk](https://gitlab.com/gitlab-com/customer-success/sa-service-desk/issues) - your go to place for pre-sales, post-sales, customer training and professional services
- **Email**: Please use the "Email, Slack, and GitLab Groups and Aliases" document for the appropriate team alias.  
- **Slack**: A short list of the primary Slack channels used by these respective teams  
    - `#sales`
    - `#marketing`
    - `#solutions_architect`
    - `#bdr-team`
    - `#sdr`
    - `#sfdc-users`
    - `#lead-questions`

## Tech Stack     

<div class="tg-wrap">
<table class="tg">
  <tr>
    <th class="tg-k6pi">Applications & Tools</th>
    <th class="tg-031e">Who Should Have Access</th>
    <th class="tg-yw4l">Whom to Contact w/Questions</th>
    <th class="tg-yw4l">Admin(s)</th>
  </tr>
  <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#avalara">Avalara</a></strong></td>
    <td>Finance Team</td>
    <td><strong>Wilson Lau (primary)</strong><br>Art Nasser</td>
    <td>Wilson Lau<br>Art Nasser</td>
  </tr>
   <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#carta">Carta</a></strong></td>
    <td>Access via GitLab credentials</td>
    <td>Paul Machle</td>
    <td>Paul Machle</td>
  </tr>
  <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#clearbit">Clearbit</a></strong></td>
    <td>Account Executives<br>Account Managers<br>Outbound SDR<br>Inbound BDR</td>
    <td><strong>JJ Cordz (primary)</strong><br>Francis Aquino</td>
    <td>JJ Cordz<br>Francis Aquino</td>
  </tr>
  <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#discoverorg">DiscoverOrg</a></strong></td>
    <td>Mid-Market AEs<br>Outbound SDR</td>
    <td><strong>Francis Aquino</strong></td>
    <td>Francis Aquino</td>
  </tr>
  <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#dogood">DoGood</a></strong></td>
    <td>Online Marketing<br>SDR Team Lead</td>
    <td><strong>Nicholas Flynn</strong></td>
    <td>Nicholas Flynn<br>JJ Cordz</td>
  </tr>
  <tr>
    <td><strong>Drift</strong></td>
    <td>Inbound BDR</td>
    <td><strong>Molly Young (primary)</strong><br>JJ Cordz (integration)</td>
    <td>JJ Cordz</td>
  </tr>
   <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#expensify">Expensify</a></strong></td>
    <td>Access via GitLab credentials</td>
    <td><strong>Wilson Lau (primary)</strong><br>Art Nasser</td>
    <td>Wilson Lau<br>Art Nasser</td>
  </tr>
  <tr>
    <td><strong>FunnelCake</strong></td>
    <td>Sr Director Demand Gen<br>Marketing OPS<br>Online Marketing</td>
    <td>&nbsp;</td>
    <td>JJ Cordz</td>
  </tr>
  <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#google-analytics">Google Analytics</a></strong></td>
    <td>Sr Director Demand Gen<br>Marketing OPS<br>Online Marketing<br>Product Marketing<br>Content Marketing</td>
    <td><strong>JJ Cordz</strong></td>
    <td>JJ Cordz</td>
  </tr>
  <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#google-tag-manager">Google Tag Manager</a></strong></td>
    <td>Online Marketing</td>
    <td>&nbsp;</td>
    <td>JJ Cordz</td>
  </tr>
  <tr>
    <td><strong>GovWin IQ</strong></td>
    <td>Sales - Federal</td>
    <td><strong>Francis Aquino</strong></td>
    <td>Francis Aquino</td>
  </tr>
  <tr>
    <td><strong>InsightSquared</strong></td>
    <td>Executives<br>Sales Leadership</td>
    <td><strong>Francis Aquino</strong></td>
    <td>Francis Aquino</td>
  </tr>
  <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#leandata">LeanData</a></strong></td>
    <td>Marketing OPS<br>Sales OPS</td>
    <td><strong>JJ Cordz (primary)</strong><br>Francis Aquino</td>
    <td>JJ Cordz<br>Francis Aquino</td>
  </tr>
  <tr>
    <td><strong>LicenseApp</strong></td>
    <td>Access via GitLab credentials</td>
    <td><strong>Francis Aquino</strong></td>
    <td></td>
  </tr>
  <tr>
    <td><strong>LinkedIn Sales Navigator</strong></td>
    <td>Sales Leadership<br>Account Executives<br>Account Managers<br>Outbound SDR<br>Inbound BDR</td>
    <td><strong>JJ Cordz</strong></td>
    <td>JJ Cordz</td>
  </tr>
  <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#marketo">Marketo</a></strong></td>
    <td>Marketing OPS<br>Online Marketing<br>Content Marketing<br>Field Marketing</td>
    <td><strong>JJ Cordz</strong></td>
    <td>JJ Cordz</td>
  </tr>
  <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#netsuite">NetSuite</a></strong></td>
    <td>Finance Team</td>
    <td><strong>Art Nasser (primary)</strong><br>Wilson Lau</td>
    <td>Art Nasser<br>Wilson Lau</td>
  </tr>
  <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#optimizely">Optimizely</a></strong></td>
    <td>Online Marketing</td>
    <td>&nbsp;</td>
    <td>JJ Cordz</td>
  </tr>
  <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#outreachio">Outreach.io</a></strong></td>
    <td>Account Executives<br>Account Managers<br>Outbound SDR<br>Inbound BDR</td>
    <td><strong>Francis Aquino (primary)</strong><br>JJ Cordz<br>Chet Backman</td>
    <td>Francis Aquino<br>JJ Cordz<br>Chet Backman</td>
  </tr>
  <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#OwnBackup">OwnBackup</a></strong></td>
    <td>Business Operations</td>
    <td><strong>Brian Flood (primary)</strong><br>Francis Aquino (secondary)</td>
    <td>Brian Flood</td>
  </tr>
  <tr>
    <td><strong>Piwik</strong></td>
    <td>Online Marketing</td>
    <td>&nbsp;</td>
    <td>Online Marketing Manager</td>
  </tr>
  <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#salesforce">Salesforce</a></strong></td>
    <td>Sales Leadership<br>Account Executives<br>Account Managers<br>Outbound SDR<br>Inbound BDR<br>Solutions Architects<br>Marketing OPS</td>
    <td>Not related to quotes or lead routing: <strong>Francis Aquino</strong><br><br>Lead Routing/Scoring/Source: <strong>JJ Cordz</strong></td>
    <td>Francis Aquino<br>JJ Cordz</td>
  </tr>
  <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#sertifi">Sertifi</a></strong></td>
    <td>Account Executives<br>Account Managers</td>
    <td><strong>Francis Aquino</strong></td>
    <td>Francis Aquino</td>
  </tr>
  <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#stripe">Stripe</a></strong></td>
    <td>Finance Team</td><td><strong>Wilson Lau</strong></td><td>Wilson Lau</td>
  </tr>
  <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#terminus">Terminus</a></strong></td>
    <td>Online Marketing</td>
    <td><strong>Emily Kyle (primary)</strong><br>JJ Cordz</td>
    <td>JJ Cordz</td>
  </tr>
  <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#unbounce">Unbounce</a></strong></td>
    <td>Marketing OPS<br>Online Marketing<br>Content Marketing<br>Field Marketing<br>Design</td>
    <td><strong>JJ Cordz</strong></td>
    <td>JJ Cordz</td>
  </tr>
  <tr>
    <td><strong>WebEx</strong></td>
    <td>Marketing OPS</td>
    <td><strong>JJ Cordz</strong></td>
    <td>JJ Cordz</td>
  </tr>
  <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#xactly">Xactly</a></strong></td>
    <td>Sales Leadership<br>Finance Team</td><td><strong>Francis Aquino (primary)</strong><br>Wilson Lau</td>
    <td>Francis Aquino<br>Wilson Lau</td>
  </tr>
  <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#zendesk">Zendesk</a></strong></td>
    <td>Access via Salesforce</td>
    <td><strong>Lee Matos</strong></td>
    <td>Francis Aquino</td>
  </tr>
  <tr>
    <td><strong>Zoom</strong></td>
    <td>Access via GitLab credentials</td>
    <td><strong>PeopleOPS</strong></td>
    <td>PeopleOPS</td>
  </tr>
  <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#zuora">Zuora</a></strong></td>
    <td>Access via Salesforce</td>
    <td><strong>Wilson Lau (primary)</strong><br>Francis Aquino (secondary)</td>
    <td>Francis Aquino</td>
  </tr>
</table>
</div>

Also see "Operations License Tracking & Contract Details" which can be found on the Google Drive.

## Glossary   

| Term | Definition |
| :--- | :--- |
| Accepted Lead | A lead a Sales Development Representative or Business Development Representative agrees to work until qualified in or qualified out |
| Account | An organization tracked in salesforce.com. An account can be a prospect, customer, former customer, integrator, reseller, or prospective reseller |
| AM | Account Manager |
| AE | Account Executive |
| APAC | Asia-Pacific |
| BDR | Business Development Representative |
| CS | Customer Success |
| EMEA | Europe, Middle East and Asia |
| Inquiry | an Inbound request or response to an outbound marketing effort |
| IQM | Initial Qualifying Meeting |
| MQL | Marketo Qualified Lead - an inquiry that has been qualified through systematic means (e.g. through demographic/firmographic/behavior lead scoring) |
| NCSA | North, Central, South America |
| Qualified Lead | A lead a Sales Development Representative or Business Development Representative has qualified, converted to an opportunity and assigned to a Sales Representative (Stage `0-Pending Acceptance`) |
| RD | Regional Director |
| Sales Admin | Sales Administrator |
| [SAO] | Sales Accepted Opportunity - an opportunity Sales agrees to pursue following an Initial Qualifying Meeting |
| SDR | Sales Development Representative |
| SLA | Service Level Agreement |
| SCLAU | Abbreviation for SAO (Sales Accepted Opportunity) Count Large and Up |
| SQO | Sales Qualified Opportunity |
| TEDD | Technology, Engineering, Development and Design - used to estimate the maximum potential users of GitLab at a company |
| Won Opportunity | Contract signed to Purchase GitLab |      

## Customer Lifecycle

A customer lifecycle is a term used to track the different milestones prospective customers go through as they learn about GitLab and interact with our sales and marketing teams. Each subsequent milestone is a subset of the previous milestone, and represents a progression from not knowing GitLab to being a customer (and fan) of GitLab.

At the highest level of abstraction, we use the terms `lead` `opportunity` and `customer` to represent a person's progression towards becoming a customer of GitLab. Those three terms also correspond to record types in salesforce.com

Lead => Opportunity => Customer

However, there are more granular steps within the above milestones that are used to track the above process with more precision. They are tracked as follows:

| Funnel stage | Record Type | Status or Stage |
|---------------|--|----------------------|-------------------|
| Raw | [Lead or Contact] | Raw |
| Inquiry | [Lead or Contact] | Inquiry |
| Marketo Qualified Lead | [Lead or Contact] |  MQL |
| Accepted Lead | [Lead or Contact] |  Accepted |
| Qualifying | [Lead or Contact] |  Qualifying |
| Pending Acceptance | [Opportunity] | 0 - Pending Acceptance |
| [Sales Accepted Opportunity] | [Opportunity] | 1 - Discovery |
| Sales Qualified Opportunity | [Opportunity] | 2 Scoping - 5 Negotiating |
| Customer | [Opportunity] | 6-Closed Won |

For the definition of the stage please click the record type link.

When going from Qualifying to Qualified Lead the lead is duplicated to an opportunity, and the lead is set to qualified and not being used anymore.  

[Lead or Contact]: https://about.gitlab.com/handbook/marketing/marketing-sales-development/sdr/inbound/#statuses
[Opportunity]: https://about.gitlab.com/handbook/sales/#opportunity-stages
[Sales Accepted Opportunity]: https://about.gitlab.com/handbook/marketing/marketing-sales-development/sdr/outbound/#acceptedopp


## Lead/Contact Management   

### MQL Definition  
A Marketo Qualified Lead is a lead that has reached a certain threshold, we have determined to be 90 points, based on demographic, firmographic and/or behavioral information. The "MQL score" (search on the google drive for more information) is comprised of various actions and/or profile data that are weighted with positive or negative point values.  
Any record with a `Person Score` greater than or equal to 50 points will be inserted into the routing flow. Using LeanData every time a `Person Score` is updated, LeanData will run a check to see if the record needs to be processed through the flow.    

### Segmentation  

#### Size   
Strategic = 5000+ users  
Large = 751-4999 users   
Mid-Market = 101-750 users   
Small-Medium Business (SMB) = 1-100 users   

#### How Segments Are Determined

Sales Segmentation can be determined by a few different criteria in Salesforce:

On the lead object, it can be defined by the following fields:
1. `Number of Developers Bucket` field, which is captured on various web forms including EE trial signups
1. `How Many Developers Do You Have?` qualifying question, which is asked during the qualification process

On the lead object, the Sales Segmentation will be determined by the larger of the two values. So if the `Number of Developers` field is `>100` but the `How Many Developers Do You Have?` field is `200`, the lead record will be categorized as `Mid-Market`.

On the account object, it can be defined by the following fields:
1. `How Many Developers Do You Have?` field, which is populated on the opportunity object after lead conversion
1. `IT + TEDD employees` The sum of IT and [TEDD](https://discoverorg.com/discoverorg-sales-intelligence-platform/data-sets-for-marketing-sales-and-staffing/technology-engineering-design-and-development-tedd-dataset/) employees from DiscoverOrg.
1. `Number of Licenses` field, which is populated from Zuora and captures the number of licenses that the customer has purchased
1. Manual override of the `Potential EE Users` field. If you decide to overwrite the `Potential EE Users` field, please note that it must be more than the `Number of Licenses` since a customer cannot have less potential users than they purchased.

Note that the default value for leads and accounts where all of the controlling fields are `NULL` or `0` is `Unknown`.

#### Parent Sales Segmentation

In addition to the current account Sales Segmentation, the Parent Sales Segmentation Field is displayed on the account record as well. The purpose of this field is to show the potential users of the parent account, as it is possible that the current account you are viewing has a potential user count of 100, for example- as it may be a specific location, business unit, or department- but that the parent account may have potential users numbering in the thousands. Please note that the child accounts' Potential EE Users do not roll up to the parent account.

#### Region/Vertical   
**Asia Pacific ([APAC](https://docs.google.com/document/d/1Ar0Y49XF0pnvWjhr5-jr0MNKdxfRY2FkS4x9y7KCZw8/edit#))**: Regional Director - Michael Alessio   
**Europe, Middle East and Africa ([EMEA](http://genesisworld.com/assets/uploads/2014/11/map_EMEA.jpg))**: Regional Director - Richard Pidgeon   
**North America - [US East](https://dev.gitlab.org/gitlab/salesforce/issues/118)**: Regional Director - Mark Rogge  
**North America - [US West](https://dev.gitlab.org/gitlab/salesforce/issues/118)**: Regional Director - Haydn Mackay  
**Public Sector**: Director of Federal Sales - Paul Almeida   

In Marketo we have modified the country values to match Salesforce to allow for seamless syncronization between the two systems. When creating new forms in Marketo, you can use the "Master Country List" (viewable on the Google Drive), copying column A into the form builder.

### Initial Source  
`Initial Source` is first "known" touch attribution or when a website visitor becomes a known name in our database, it should never be changed or overwritten. If merging records, keep the `Initial Source` that is oldest (or set first). When creating Lead/Contact records and you are unsure what `Initial Source` should be used, ask in the #Lead-Questions Slack channel.   

### Lead & Contact Statuses
The Lead & Contact objects in Salesforce have unified statuses with the following definitions. If you have questions about current status, please ask in #Lead-Questions channel on Slack.

| Status | Definition |
| :--- | :--- |
| Raw | Untouched brand new lead |
| Inquiry | Form submission, meeting @ trade show, content offer |
| MQL | Marketo Qualified through systematic means |
| Accepted | Actively working to get in touch with the lead/contact |
| Qualifying | In 2-way conversation with lead/contact |
| Qualified | Progressing to next step of sales funnel (typically OPP created & hand off to Sales team) |
| Unqualified | Contact information is not now or ever valid in future; Spam form fill-out |
| Nurture | Record is not ready for our services or buying conversation now, possibly later |
| Bad Data | Incorrect data - to potentially be researched to find correct data to contact by other means |
| Web Portal Purchase | Used when lead/contact completed a purchase through self-serve channel & duplicate record exists |

### Lead Routing
The lead routing rules are determined by the incoming source (`EE Trial`, `Contact Us`, etc) and factors related to `Sales Segmentation` as well as `Region`.

#### Contact Us Requests     
All `Contact Us` requests must be followed up within **one (1) business day** (service level agreement SLA) and that follow up must be tracked as an activity on the record within Salesforce.

`Strategic`, `Large` and `Mid-Market` Sales Segments: Routed directly to the Strategic Account Leader (SAL) or Account Executive (AE) that owns the account in Salesforce.  
- At the Account Owner's discretion, they may reassign `Contact Us` follow up to an SDR, BDR or may follow-up directly.   
- Account Owner is responsible to ensure communication happens internally and externally within the SLA timeframe  
- Account Owner is responsible to user the same method of determining `Sales Qualified Amount` so it does not impact our planning in the Revenue Model. This means setting the amount to reflect the number of seats the prospect initially inquires about not the amount that will be reflected in the initial order.   

`SMB` or `Unknown` Sales Segments: Routed to the Business Development Representatives (BDR) based on region (EMEA, NCSA, APAC)  

US Federal and Government: Routed to the Federal Accounts team for follow up and response

#### Enterprise Trial Requests   
Enterprise Edition (EE) trials can be requested through [web form](https://about.gitlab.com/free-trial/) or in-product request and default length is thirty (30) days. Trial leads are routed to the inbound BDR team based on region, regardless of sales segment.
Regional distribution of EE Trial leads are:   
   - APAC: If Korean, route to Conor Brady; all others US/APAC BDR Round Robin  
   - EMEA: EMEA BDR Round Robin  
   - NCSA: US/APAC BDR Round Robin  
   - Federal, Government, Non-Profit, Education: US/APAC BDR Round Robin

#### List Imports
You may obtain a list from an external source. If you would like the list uploaded, you must make sure that the following initial data cleanup is completed:
   - It must be in either .csv, .xls, or .xlsx format
   - Address fields are separated into their own fields: Address 1, Address 2, City, State/Province, Zip/Postal Code, Country  
   - Name is separated into first and last name fields
   - If you are not the owner of the records, you must provide a column that includes the Owner for each record. You will be the owner for any row where owner is NULL. 

Once the initial data cleanup is complete, you will send the file to Sales and Marketing Operations. You must include the following details on your email:
   - Source of the list (trade show, external database, etc)
   - Desired Campaign Name
   - Any marketing opt-outs.

File will be reviewed and may be returned if it does not meet the initial data cleanup requirements, or may have other issues not mentioned above. Once the file is accepted, please allow for a five (5) day turnaround time. 
 

### Criteria for Sales Accepted Opportunity (SAO)

The following criteria are **required** for all SAOs:

* Confirmation of right person/right profile - the prospect you are speaking with is involved in any project or team related to the potential purchase of GitLab, either as an evaluator, decision maker, technical buyer, or influencer and is interested in hearing more about GitLab.
* You do not need full BANT to deem an opportunity as sales aqualified and to move it to stage 1-Discovery. All that stage 1-Discovery means is that you are in the process of prospect discovery: you are qualifying the opportunity.

* During the BDR/SDR qualification, several data points may be captured, but are **not required** for an SAO:
  * The immediate number of seats or users that the prospect may be interested in purchasing.
  * The prospect's preference on how they would like to deploy GitLab (hosted or self-hosted).
  * Technology stack- what solutions are in place today.
  * Solutions to be replaced- from the list of tools that are in place, which of those solutions are up for replacement.
  * Competition- a list of solutions that we will compete with.

Once the opportunity is created, it should be set to "0-Pending Acceptance" stage. Please see the [Opportunity Stage](https://about.gitlab.com/handbook/sales/#opportunity-stages) for stage definitions and activities.

### Opportunity Stages

**00-Pre Opportunity**- This stage should be used when an opportunity does not meet our opportunity criteria. However, there is a potential for business, and it should be tracked for possible business.
* What to Complete in This Stage:
  * Schedule discovery call with prospect to determine if there is an opportunity to pursue; or
  * If there is no opportunity then the stage would be updated to 0-Unqualified.

**0-Pending Acceptance**: This is the initial stage once an opportunity is created.
* What to Complete in This Stage:
  * For BDR or SDR sourced opportunities, the opportunity meets [Sales Accepted Opportunity criteria](#criteria-for-sales-accepted-opportunity-sao).
  * The BDR or SDR has scheduled a call via Google Calendar, sent invites, created an event on the account object, named the event: Gitlab Introductory Meeting - {{Account Name}}
  * Once it is confirmed that the opportunity meets our Sales Accepted Opportunity criteria, the SAL or AE should move the opportunity to the next stage. The date the opportunity moves from this to the next stage in the sales cycle will populate the `Sales Accepted Date` field on the opportunity record.
  * If the details on the opportunity do not meet our Sales Accepted Opportunity criteria, the SAL or AE should move the opportunity to an `8-Unqualified` stage (this is the only time an opportunity can move into `8-Unqualified` stage).

**1-Discovery**: Uncover as much intelligence about the project as you can, which will be confirmed at later stages throughout the sales cycle.
* What to Complete in This Stage:
  * Begin filling out [MEDDPIC](/handbook/sales/#capturing-meddpic-questions-for-deeper-qualification)
  * Send Plan Letter/Recap Email to Attendees- [Example](https://docs.google.com/document/d/16Gurj_MVREmKoqXTdB1F0OQ3eyq1gzbTNU8LNHHuoEM/edit)
  * Scheduled Scoping Call
  * Should the opportunity progress from `1-Discovery` to the next stage (not 7-Closed Lost or 9-Duplicate), it will be considered a `Sales Qualified Opportunity`. The following values are entered once the opportunity progresses from this stage:
     * `Sales Qualified` is True.
     * `Sales Qualified Date` is the date the opportunity moves from this stage to the next open or won stage.
     * `Initial IACV` captures the value in the `Incremental ACV` field. `Initial IACV` is a snapshot field that will not change, even when the `Incremental ACV` field is updated and will be used for `Deal Size` analysis.

**2-Scoping**: Uncover business challenges/objectives, the competitive landscape, realizing fit.
* What to Complete in This Stage:
  * Complete a Demo (Optional)
  * Schedule a Technical Evaluation Call
  * Confirm and collect new [MEDDPIC](/handbook/sales/#capturing-meddpic-questions-for-deeper-qualification) information.

**3-Technical Evaluation**: Confirming technical requirements. A proof-of-concept (POC) might occur at this stage. This is also the stage to confirm information before a proposal is delivered.
* What to Complete in This Stage:
  *  Enter POC Notes and POC Success Criteria (if applicable) and enter into the POC Notes and POC Success Criteria fields on the opportunity.
  *  Confirm *Technical Requirements, POC Scope*
  *  Confirm *Technical Win/POC Success*
  *  Confirm and collect new [MEDDPIC](/handbook/sales/#capturing-meddpic-questions-for-deeper-qualification) information.

**4-Proposal**: Business and technical challenges and been uncovered and resolved. A proposal is drafted and delivered to the prospect.
* What to Complete in This Stage:
  * Uncover Bill to/Sold to information; Billing Details, Vendor Registration Process
  * Deliver formal contract to the prospect
  * An MSA may be delivered separately
  * Clear understanding of purchase/contract review process and a close plan (actions to be taken, named of people to complete actions and dates for each action) documented in the Purchasing Plan field.

**5-Negotiating**: The prospect or customer has received the proposal and is in contract negotiations.
* What to Complete in This Stage:
  * Agreement on business terms
  * All proposals should include the standard GitLab [Terms](https://about.gitlab.com/terms/)
  * Modifications will not be accepted to the standard terms for any opportunity that is less than $25k, or for Standard edition.
  * If the above threshold is met, requests for modifications to the standard terms should be sent to Legal through SalesForce, using Chatter. Be sure to include the relevant Account and Opportunity information in the request.
  * If the Account is seeking to use their own paper, requests should be made to legal using the [Legal Issue Tracker](mailto:legal@gitlab.com).

**6-Awaiting Signature**: The prospect or customer has verbally agreed to the terms and conditions outlined in the proposal and has submitted for signature.
* What to Complete in This Stage:
  * Received signed order form, which signals agreement of all pricing and legal terms.
  * Obtain a purchase order, if applicable.
  * Work with GitLab AR to deliver any tax and/or complete any vendor registration processes.
  * Ensure all relevant documents, MSA, PO, and other forms uploaded to SFDC in the Notes and Attachments section of the opportunity record.
  * EULA has been accepted by end-user organization (if applicable).
  * If this is a large/strategic account, or if IACV is +$12,000, enter `Closed Won Details` summarizing why we won the opportunity.
  * Subscription created in Zuora.
  * Opportunity has been submitted for Finance approval.

**7-Closed Won**: Congratulations!! The terms have been agreed to by both parties and the quote has been approved by Finance.
* What to Complete in This Stage:
  * Introduce Customer Success/Account Management (if applicable)
  * Set a calendar reminder for 30 day follow up
  * If applicable, initiate Customer Onboarding or Premium Support onboarding

**8-Closed Lost**: An opportunity was lost and the prospect/customer has decided not to pursue the purchase of GitLab.
* What to Complete in This Stage:
  * Select all applicable Closed Lost Reasons
  * In the `Closed Lost Detail`, enter as much detail as you can as to why we lost the deal. For example:
     * If they selected a competitor, why? Was it due to features or pricing?
     * If decided not to move forward with a project, what were the reasons? Did they not understand the value? Was there not a compelling event or reason?
     * Again, please be as thorugh as you can as this information will prove valuable as we learn from these experiences.
  * Please note that for new business deals where the opportunity is with a Large/Strategic account OR the Incremental Annual Contract Value (IACV) is equal or greater than USD 12,000, then a notification will be sent to the [#lost-deals](https://gitlab.slack.com/messages/C8RP2BBA7) Slack channel.
  * Uncover a time for follow up (incumbent solution contract expiration date)

**9-Unqualified**: An opportunity was never qualified.
* What to Complete in This Stage:
  * Update Reason for Closed Lost and add any pertinent notes as to why the opportunity was not qualified.
  * A notification will be sent to BDR or SDR Team Lead and a feedback session should be scheduled between AE and Team Lead.

**10-Duplicate**: A duplicate opportunity exists in the system.

#### Opportunity Stage Movement Considerations
Note that once you qualify an opportunity via our standard qualification process, you cannot revert an opportunity back to the following stages: 00-Pre Opportunity, 0-Pre AE Qualified, or 8-Unqualified. If you need to revert an opportunity you've previously qualified to one of these stages, please contact Sales Operations and we can determine why the opportunity (once qualified) is no longer qualified.

#### Reverting an Opportunity to a Previous Stage
If a previously met criteria has become unmet, you are required to revert back to the latest stage where all activites were completed. For example, if a prospect had previously signed off on GitLab from a technical standpoint, requested a quote and has begun contract negotiations, you would set the opportunity to "5-Negotiating". However, if at any point during the negotiations, additional technical questions or requirements arise that result in a re-evaluation of GitLab's technical capabilities, you would revert the opportunity back to "3-Technical Evaluation". After the opportuniy has once again met the stage completion criteria, you are able to move the opportunity to either "4-Proposal" if a new order form was created as a result of the additional technical requirements, or back to "5-Negotiating" if no changes were made to the original order form.


## Rules of Engagement  

Inbound BDRs mainly work within the LEAD object in Salesforce. These records are the result of the following activities:   
- Enterprise Trial request (either web form or in product) all Sales Segments  
- EE Live Demo Webcast all Sales Segments
- Contact Request in `SMB` or `Unknown` sales segment  
- Engage in `Web Chat` through the Drift bot on select pages

Outbound SDRs mainly work within the CONTACT & ACCOUNT objects in Salesforce. These records are the result of the following activities:   
- Conference Scanned or Field Event Leads   
- CE Usage Ping data  
- List Imports from DiscoverOrg associated with their named accounts     

**Directionally**: If someone is exhibiting bonafide interest in evaluating EE that person is routed to an inbound BDR and is handled with the existing qualification process.  
If someone has shown interest in educational topics like high level webinars, white papers, or visited us at a tradeshow booth AND they are from a `Large` or `Strategic` account these are handled by the outbound SDR for followup.    

LEAD/CONTACT Records with the `Initial Source` of `GitLab.com` are **not** to be engaged, prospected or targeted unless they have taken a handraising 'active' activity, as defined below.

### Active vs. Passive

`Initial Source` cannot be used to determine if a lead is 'active' or 'passive' since the Initial Source is set upon first touch attribution; therefore, looking at the `Last Interesting Moment` field is the primary field used to begin determining if a record is actively being worked. Reviewing the `Activity History` in Salesforce is another factor considered when evaluating 'active' or 'passive'.  

A LEAD or CONTACT is considered 'Active' if they have taken an `EE Trial`, attended an EEP Demo webcast and/or engaged `Web Chat`, these are all handraising 'active' activities. These types of records are worked by the inbound BDR team. The record is considered 'Active' for entire duration of EE trial, plus 30 days after `EE Trial End Date`.   

Situational Handling of Leads  
- If a LEAD completes an **inbound** action, matches an owned ACCOUNT in the same region and there is an **open opportunity**, the BDR needs to make the Account Owner aware BEFORE outreach to the LEAD.     
      - If LEAD matches existing CONTACT record on ACCOUNT with **open opportunity** the BDR will hand off to Account Owner. 
      - If LEAD *does not* match existing CONTACT record on ACCOUNT with **open opportunity** - BDR will chatter Account Owner to best determine coordinated outreach in attempt to identify if LEAD is related to open opportunity. 
- If a LEAD completes an **inbound** action, matches an owned ACCOUNT in the same region, there is **not** an open opportunity, the LEAD matches a known CONTACT on the account and there is outbound activity recorded within the past 60 days, the BDR needs to reach out to the Account Owner and handoff the LEAD.    
- IF a LEAD completes an **inbound** action, matches an owned ACCOUNT in the same region, there is **not** an open opportunity, the LEAD matches a knonw CONTACT on the account and there is **not** outbound activity recorded within the past 60 days, the BDR completes standard qualification outreach to the LEAD.    
- If a LEAD completes an **inbound** action, matches an owned ACCOUNT in the same region, there is **not** an open opportunity, the LEAD does not match any known CONTACT on the account, and there is no outbound activity recorded on the account in the past 60 days, the BDR completes standard qualification outreach to LEAD.   
- If a LEAD takes an `EE Trial` or has engaged in `Web Chat` and completes a `Contact Request`, the inbound BDR who is working the `EE Trial` or `Web Chat` retains ownership of the LEAD record, regardless of Sales Segment.  

## Record Ownership   
Contacts on accounts owned by a member of the Field Sales Team (RD/AE/AM/SAL), will be owned by the named Account Owner (i.e both the Account and Contact ownership will match). When an SDR is assigned to an Account to support and assist with outreach, the SDR will be added to the `GitLab Team` on the Account object within SFDC, which then populates down to the related Contact records.    

Records, both `Lead` and `Contact`, need to be synced to Outreach to ensure email activity is properly mapped back to `Activity History` in SFDC. The owner of the record in SFDC **does not** need to match the owner in Outreach.

## Types of Accounts

### Accounts Created in Salesforce utilizing CE Usage Ping Data   
The [CE Usage ping](https://docs.gitlab.com/ee/user/admin_area/settings/usage_statistics.html) provides GitLab with some limited insight into how end users are utilizing the platform. The raw information is cleaned, enriched and then pushed to SFDC as an Account by the BizOPS team.  

If there is not an existing account match in Salesforce, a new account record will be created with the following information populated:    

| SFDC Field | Default Value |
|---|---|
| Account Name |  |
| Number of Employees |  |
| Billing Street |  |
| Billing City |  |
| Billing Zip |  |
| Billing Country |  |
| Account Type | `Prospect - CE User` |
| Account Website |  |
| Industry | Populated by Clearbit |
| Active CE Users | Populated by Usage Ping |
| CE Instances | Populated by Usage Ping |
| Account Ownwer | Sales Admin by Default |
| Using CE | Checked True |

**Process**  
1. Sales Team members can use this data to proactively identify `Prospect - CE User` accounts that fit their target segement(s). Accounts owned by `Sales Admin` can be adopted by a Sales Team member changing ownership in Salesforce. The adoption of any `Sales Admin` owned records will trigger an email alert that is sent to the Account Research Specialist for transparency and awareness of what account records have been claimed.
2. The Account Research Specialist will be responsible for reviewing the `Prospect - CE User` accounts on a regular basis to determine additional account records that should be worked either by a Sales Team member or Outbound SDR.
3. When an account record has been identified for follow up, the Account Research Specialist will work with the appropriate Regional Director (RD) to determine Outbound SDR assignment based on work load and available capacity.
4. The assigned Outbound SDR will work the `Prospect - CE User` account the same as any other known `CE User` account leveraging the tools at their disposal (DiscoverOrg, LinkedIn Sales Navigator, etc) to add contacts to the account record and populate the firmographic profile of the account.   

## Weekly Marketing Reporting

The Online Marketing team is in charge of updating reports and dashboards for the weekly marketing meetings. The current reporting that is being used can be found in Google Drive by searching for "Quarterly Marketing Dashboard".

To access numbers for the reports, you can use the following links:
- [Web Traffic](https://analytics.google.com/analytics/web/#report/acquisition-channels/a37019925w65271535p67064032/)
- [Unique Visitors](https://analytics.google.com/analytics/web/#report/visitors-overview/a37019925w65271535p67064032/)
- [Respondents (leads)](https://app-ab13.marketo.com/#AR1358A1)
- [MQLs](https://app-ab13.marketo.com/#AR1365A1) (This is a little complicated as this report actually gives you lead creation date, so you need to go to `Smart List` -> `View Qualified Leads` then export and use a pivot table in Excel to get the actual numbers. I'm open to ideas if there's a better way to get the MQL numbers)
- [SQO #](https://na34.salesforce.com/00O61000003no6p)
- [SQO $ Value](https://na34.salesforce.com/00O61000003no6p)
- [Won #](https://na34.salesforce.com/00O61000003no6z)
- [Won $ Amount](https://na34.salesforce.com/00O61000003no6z)

## Reporting on Large and Up Sales Segment Opportunity Creation

An area of focus is the [large and strategic sales segments](/handbook/sales/#market-segmentation). For visibility into performance in these market segments we use a [Large and Up Opportunity Performance dashboard](https://na34.salesforce.com/01Z61000000J0h1) in salesforce.com. It tracks the measures [pending acceptance](/handbook/business-ops/#customer-lifecycle/) opportunities, [SAO](/handbook/business-ops/#customer-lifecycle/), and [SQO](/handbook/business-ops/#customer-lifecycle/).

The dashboard analyzes opportunity measures by the dimensions `initial source type` [sales segmentation](/handbook/sales/#market-segmentation) and [sales qualification source](https://about.gitlab.com/handbook/sales/#tracking-sales-qualified-source-in-the-opportunity). We set our demand generation targets based on SAO count in large and up segments by `initial source type` and `sales qualification source` as follows:

- BDR Generated - opportunity is created by a BDR qualifying inbound demand. Sales qualification source is `BDR Generated`.
- SDR Generated - opportunity is created by SDR prospecting. Sales qualification source is `SDR Generated`.
- AE Generated, Marketing assisted - opportunity is created by an AE or SAL and is associated with a contact that has responded to one or more marketing programs. Sales qualification source is `AE generated` and `Initial source` is not `AE Generated`.
- AE Generated, Cold calling - opportunity is created by an AE or SAL but is not associated with any contacts that have responded to marketing programs. Sales qualification source is `AE generated` and `Initial source` is `AE Generated`.
